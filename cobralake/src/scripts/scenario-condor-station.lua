-- Header {{{1
----------------------------------------------------------------------------------
--
--  CONDOR STATION
--
--  *   As usual, load MOOSE first then this script. To be sure this happens,
--      use time-sensitive triggers. E.g.,
--      +   MOOSE gets loaded on MISSION START or ONCE with TIME MORE (1) as a condition
--      +   Then the COBRALAKE script, "cobralake.lua" gets loaded ONCE with TIME MORE (2)
--          as a condition.
--      +   Then this script gets loaded ONCE with TIME MORE (4) as a condition.
----------------------------------------------------------------------------------
-- }}}1

-- Mission Settings {{{1
----------------------------------------------------------------------------------

if maxConcurrentEnemyInterdictionMissions == nil then
    maxConcurrentEnemyInterdictionMissions = 5
end

if enemyInventoryAllocation == nil then
    enemyInventoryAllocation = {
        mi24  = 24,
        mig21 = 24,
        mig27 = 24,
        mig29 = 24,
        su17  = 24,
        su24  = 24,
        su25  = 24,
        su27  = 36,
        su30  =  0,
        tu22  = 36,
        tu142 = 24,
    }
end

if alliedInventoryAllocation == nil then
    alliedInventoryAllocation = {
        f14 = 18,
    }
end

if armageddonSettings == nil then
    armageddonSettings = {}
end

if armageddonSettings.isAllowBackgroundTrigger == nil or armageddonSettings.isAllowBackgroundTrigger then
    if armageddonSettings.backgroundRate == nil then
        armageddonSettings.backgroundRate = 1/(60 * 20)
    end
else
    armageddonSettings.backgroundRate = 0.0
end
if armageddonSettings.zoneViolationTriggerProbability == nil then
    armageddonSettings.zoneViolationTriggerProbability = 1.0
end
if armageddonSettings.onReturnToCarrierProbability == nil then
    armageddonSettings.onReturnToCarrierProbability = 0.75
end

alliedSupportInventoryAllocation = {
    kc135 = 4,
    kc130 = 4,
    e2d = 4,
    s3b = 4,
}

-- }}}1

-- }}}1

-- cobralake.ScenarioRunner  {{{1
----------------------------------------------------------------------------------

-- Preamble -- {{{2

cobralake.ScenarioRunner = {}
cobralake.ScenarioRunner.__index = cobralake.ScenarioRunner

setmetatable(cobralake.ScenarioRunner, { -- {{{3
    __call = function (cls, ...)
        return cls.new(...)
    end,
})
--}}}3

-- }}}2

-- Initialization -- {{{2
function cobralake.ScenarioRunner.new(init)
    local self = setmetatable({}, cobralake.ScenarioRunner)

    -- Mission Tuning Parameters -- {{{4
    -- Populate from global parameters if available, fallback to default
    self.scenarioParams = {
        enemyMissionAlertMessageDelayRange  = { missionAlertMessageDelayMin           or  1 * 60                , missionAlertMessageDelayMax           or  3 * 60                }  ,
        interdictionInitialTimeRange     =    { interdictionInitialTimeRangeMin       or  1 * 60                , interdictionInitialTimeRangeMax       or  2 * 60                }  , -- does not matter, as (a) we start right away; and total active is restricted by the max missions
        interdictionWaitingTimeRange     =    { interdictionWaitingTimeRangeMin       or  5 * 60                , interdictionWaitingTimeRangeMax       or  10 * 60               }  , -- does not matter, as (a) we start right away; and total active is restricted by the max missions
        antiCarrierStrikeInitialTimeRange   = { antiCarrierStrikeInitialTimeRangeMin  or  2 * 60                , antiCarrierStrikeInitialTimeRangeMax  or  3 * 60                }  ,
        antiCarrierStrikeWaitingTimeRange   = { antiCarrierStrikeWaitingTimeRangeMin  or  5 * 60                , antiCarrierStrikeWaitingTimeRangeMax  or  20 * 60               }  ,

        maxConcurrentEnemyInterdictionMissions = maxConcurrentEnemyInterdictionMissions,

        enemyInventoryAllocation = enemyInventoryAllocation,
        alliedInventoryAllocation = alliedInventoryAllocation,
        alliedSupportInventoryAllocation = alliedSupportInventoryAllocation,

        armageddonTriggerDelayRange   = {1 * 60, 3 * 60},
        armageddonDetectionDelayRange = {1, 60},
        armageddonResponseDelayRange  = {3 * 60, 5 * 60},

    } -- self.scenarioParams
    -- }}}4

    -- Allied Forces  -- {{{4
    self.alliedInventory = self:buildInventories(self.scenarioParams.alliedInventoryAllocation)
    self.alliedCarrierGroup = GROUP:FindByName("Allied CVBG")
    if self.alliedCarrierGroup == nil then
        error("ERROR: CARRIER GROUP NOT FOUND")
    end
    self.alliedCarrierUnitName = "CVN"
    self.alliedCarrierUnit = UNIT:FindByName(self.alliedCarrierUnitName)
    self.recoveryTankerGroupName = "CV Recovery Tanker"
    self.alliedCarrierCapGroupName = "Allied Carrier CAP"
    self.alliedCarrierCapStationDefinitions = {
            {
                callsignNameIndex = 2,
                radioFrequency={303.100, radio.modulation.AM},
                capStationName="DALLAS",
                patrolZoneName="East Allied CAP Patrol Zone",
                engageZoneName="East Allied CAP Engage Zone",
                isAutostart=true,
                isRepeatByDefault=true,
            },
            {
                callsignNameIndex = 1,
                radioFrequency={303.250, radio.modulation.AM},
                capStationName="DURANGO",
                patrolZoneName="North Allied CAP Patrol Zone",
                engageZoneName="North Allied CAP Engage Zone",
                isAutostart=true,
                isRepeatByDefault=true,
            },
            {
                callsignNameIndex = 3,
                radioFrequency={309.225, radio.modulation.AM},
                capStationName="DIEGO",
                patrolZoneName="West Allied CAP Patrol Zone",
                engageZoneName="West Allied CAP Engage Zone",
                isAutostart=true,
                isRepeatByDefault=true,
            },
            {
                callsignNameIndex = 4,
                radioFrequency={313.050, radio.modulation.AM},
                capStationName="CONDOR",
                spawnZoneName="Objective Allied CAP Spawn Zone",
                patrolZoneName="Objective Allied CAP Patrol Zone",
                engageZoneName="Objective Allied CAP Engage Zone",
                isAutostart=true,
                isRepeatByDefault=true,
            },
    }
    self.alliedPerpetualAssetGroupNames = {
        {
            assetGroupName = "South Support Tanker",
            maxUnitsAliveAtAnyTime = 1,
            maxGroupsSpawnedInTotal = self.scenarioParams.alliedSupportInventoryAllocation.kc135,
            callsignNameIndex = 3,
            callsignNumber = 1,
        },
        {
            assetGroupName = "North Support Tanker",
            maxUnitsAliveAtAnyTime = 1,
            maxGroupsSpawnedInTotal = self.scenarioParams.alliedSupportInventoryAllocation.kc135,
            callsignNameIndex = 3,
            callsignNumber = 2,
        },
        {
            assetGroupName = "Forward Support Tanker",
            maxUnitsAliveAtAnyTime = 1,
            maxGroupsSpawnedInTotal = self.scenarioParams.alliedSupportInventoryAllocation.kc130,
            callsignNameIndex = 3,
            callsignNumber = 3,
        },
        {
            assetGroupName = "AWACS E2D",
            maxUnitsAliveAtAnyTime = 1,
            maxGroupsSpawnedInTotal = self.scenarioParams.alliedSupportInventoryAllocation.e2d,
        },
    }
    -- }}}4

    -- Enemy Mission Asset Inventory Managers -- {{{4
    self.enemySpawnAliasIndex = 0
    self.enemyPackageCompositions = {
        "mi24",
        "mig21",
        "mig27",
        "mig29",
        "su27",
        "su25",
        "su17",
        "su24",
        "tu22",
        "tu142",
    }
    self.enemyInventory = self:buildInventories(self.scenarioParams.enemyInventoryAllocation)

    -- }}}4

    -- Enemy Package Compositions  -- {{{4
    self.enemyPackageCompositions = {}
    self.enemyPackageCompositions.heloGroundAttack = {
        {"RED A2G Mi-24V ABK x2" ,   self.enemyInventory.mi24, "RED FARP 1a", "RED Land Interdiction Zone 2"},
        {"RED A2G Mi-24V ABK x2" ,   self.enemyInventory.mi24, "RED FARP 1a", "RED Land Interdiction Zone 3"},

        {"RED A2G Mi-24V ABK x2" ,   self.enemyInventory.mi24, "RED FARP 1b", "RED Land Interdiction Zone 2"},
        {"RED A2G Mi-24V ABK x2" ,   self.enemyInventory.mi24, "RED FARP 1b", "RED Land Interdiction Zone 4"},

        {"RED A2G Mi-24V ABK x2" ,   self.enemyInventory.mi24, "RED FARP 1c", "RED Land Interdiction Zone 2"},
        {"RED A2G Mi-24V ABK x2" ,   self.enemyInventory.mi24, "RED FARP 1c", "RED Land Interdiction Zone 1"},

        {"RED A2G Mi-24V ABK x2" ,   self.enemyInventory.mi24, "RED FARP 2a", "RED Land Interdiction Zone 2"},
        {"RED A2G Mi-24V ABK x2" ,   self.enemyInventory.mi24, "RED FARP 2a", "RED Land Interdiction Zone 3"},

        {"RED A2G Mi-24V ABK x2" ,   self.enemyInventory.mi24, "RED FARP 2b", "RED Land Interdiction Zone 2"},
        {"RED A2G Mi-24V ABK x2" ,   self.enemyInventory.mi24, "RED FARP 2b", "RED Land Interdiction Zone 4"},

        {"RED A2G Mi-24V ABK x2" ,   self.enemyInventory.mi24, "RED FARP 2c", "RED Land Interdiction Zone 4"},
        {"RED A2G Mi-24V ABK x2" ,   self.enemyInventory.mi24, "RED FARP 2c", "RED Land Interdiction Zone 5"},
    }
    self.enemyPackageCompositions.groundAttack = {
        {"RED A2G Su-25 ABK x2",       self.enemyInventory.su25,  "RED A2A MiG-21Bis ABK x2", self.enemyInventory.mig21, AIRBASE.Caucasus.Sukhumi_Babushara, { {"RED Land Marshal Zone 1"}, {"RED Land Interdiction Zone 1"}, nil, nil, {"RED Land Marshal Zone 1"}, nil}, }, -- weighted x2
        {"RED A2G Su-25 ABK x2",       self.enemyInventory.su25,  "RED A2A MiG-21Bis ABK x2", self.enemyInventory.mig21, AIRBASE.Caucasus.Sukhumi_Babushara, { {"RED Land Marshal Zone 1"}, {"RED Land Interdiction Zone 1"}, nil, nil, {"RED Land Marshal Zone 1"}, nil}, }, -- weighted x2
        {"RED A2G Su-25 ABK x2",       self.enemyInventory.su25,  nil,                        nil,                       AIRBASE.Caucasus.Sukhumi_Babushara, { {"RED Land Marshal Zone 1"}, {"RED Land Interdiction Zone 1"}, nil, nil, {"RED Land Marshal Zone 1"}, nil}, }, -- weighed x2
        {"RED A2G Su-25 ABK x2",       self.enemyInventory.su25,  nil,                        nil,                       AIRBASE.Caucasus.Sukhumi_Babushara, { {"RED Land Marshal Zone 1"}, {"RED Land Interdiction Zone 1"}, nil, nil, {"RED Land Marshal Zone 1"}, nil}, }, -- weighed x2
        {"RED A2G MiG-27K x2",         self.enemyInventory.mig27, "RED A2A MiG-29S RUS x2",   self.enemyInventory.mig29, AIRBASE.Caucasus.Nalchik,           { {"RED Land Marshal Zone 2"}, nil,                              nil, nil, nil,                         nil}, },
        {"RED A2G MiG-27K x2",         self.enemyInventory.mig27, nil,                        nil,                       AIRBASE.Caucasus.Nalchik,           { {"RED Land Marshal Zone 2"}, nil,                              nil, nil, nil,                         nil}, },
        {"RED A2G Su-17M4 x2",         self.enemyInventory.su17,  "RED A2A MiG-29S RUS x2",   self.enemyInventory.mig29, AIRBASE.Caucasus.Beslan,            { {"RED Land Marshal Zone 4"}, nil,                              nil, nil, nil,                         nil}, },
        {"RED A2G Su-17M4 x2",         self.enemyInventory.su17,  nil,                        nil,                       AIRBASE.Caucasus.Beslan,            { {"RED Land Marshal Zone 4"}, nil,                              nil, nil, nil,                         nil}, },
        {"RED A2G Su-24M BOMB RUS x2", self.enemyInventory.su24,  "RED A2A MiG-29S RUS x2",   self.enemyInventory.mig29, AIRBASE.Caucasus.Mozdok,            { {"RED Land Marshal Zone 3"}, nil,                              nil, nil, nil,                         nil}, },
        {"RED A2G Su-24M BOMB RUS x2", self.enemyInventory.su24,  nil,                        nil,                       AIRBASE.Caucasus.Mozdok,            { {"RED Land Marshal Zone 3"}, nil,                              nil, nil, nil,                         nil}, },
    }
    self.enemyPackageCompositions.airAttack = {
        {"RED A2A MiG-21Bis ABK x2", self.enemyInventory.mig21, nil, nil, AIRBASE.Caucasus.Sukhumi_Babushara, { {"RED Land Marshal Zone 1"}, {"RED Land Interdiction Zone 1"}, nil, nil, {"RED Land Marshal Zone 1"}, nil}, }, -- weighted x2
        {"RED A2A MiG-21Bis ABK x2", self.enemyInventory.mig21, nil, nil, AIRBASE.Caucasus.Sukhumi_Babushara, { {"RED Land Marshal Zone 1"}, {"RED Land Interdiction Zone 1"}, nil, nil, {"RED Land Marshal Zone 1"}, nil}, }, -- weighted x2
        {"RED A2A MiG-21Bis ABK x2", self.enemyInventory.mig21, nil, nil, AIRBASE.Caucasus.Sukhumi_Babushara, { {"RED Land Marshal Zone 1"}, {"RED Land Interdiction Zone 1"}, nil, nil, {"RED Land Marshal Zone 1"}, nil}, }, -- weighted x2
        {"RED A2A MiG-21Bis ABK x2", self.enemyInventory.mig21, nil, nil, AIRBASE.Caucasus.Sukhumi_Babushara, { {"RED Land Marshal Zone 1"}, {"RED Land Interdiction Zone 1"}, nil, nil, {"RED Land Marshal Zone 1"}, nil}, }, -- weighted x2
        {"RED A2A MiG-29S RUS x2",   self.enemyInventory.mig29, nil, nil, AIRBASE.Caucasus.Nalchik,           { {"RED Land Marshal Zone 2"}, nil,                              nil, nil, nil,                         nil}, },
        {"RED A2A MiG-29S RUS x2",   self.enemyInventory.mig29, nil, nil, AIRBASE.Caucasus.Beslan,            { {"RED Land Marshal Zone 4"}, nil,                              nil, nil, nil,                         nil}, },
        {"RED A2A MiG-29S RUS x2",   self.enemyInventory.mig29, nil, nil, AIRBASE.Caucasus.Mozdok,            { {"RED Land Marshal Zone 3"}, nil,                              nil, nil, nil,                         nil}, },
        {"RED A2A Su-27 RUS x2",     self.enemyInventory.su27,  nil, nil, AIRBASE.Caucasus.Mineralnye_Vody,   { {"RED Land Marshal Zone 2"}, nil,                              nil, nil, nil,                         nil}, },
        {"RED A2A Su-27 RUS x2",     self.enemyInventory.su27,  nil, nil, AIRBASE.Caucasus.Mineralnye_Vody,   { {"RED Land Marshal Zone 2"}, nil,                              nil, nil, nil,                         nil}, },
    }

    self.enemyAntiCarrier = {}
    self.enemyAntiCarrier.routes = {
        {
            airbaseName = AIRBASE.Caucasus.Maykop_Khanskaya,
            routeZones =   {
                {"RED Maritime Marshal Zone 3",},
                {"RED Maritime Fence Zone 3"},
                nil,
                {"RED Maritime Strike Zone 4", "RED Maritime Strike Zone 5", "RED Maritime Strike Zone 6", "RED Maritime Strike Zone 7",},
                {"RED Maritime Fence Zone 3"},
            },
        },
        {
            airbaseName = AIRBASE.Caucasus.Mineralnye_Vody,
            routeZones = {
                {"RED Maritime Marshal Zone 1", "RED Maritime Marshal Zone 2"},
                {"RED Maritime Fence Zone 1", "RED Maritime Fence Zone 2"},
                nil,
                {"RED Maritime Strike Zone 1", "RED Maritime Strike Zone 2", "RED Maritime Strike Zone 3", "RED Maritime Strike Zone 4",},
                {"RED Maritime Fence Zone 2"},
            },
        },
    }
    self.enemyAntiCarrier.assets = {
        {"RED ASUW Tu-22M x4", self.enemyInventory.tu22,  nil, nil},
        {"RED ASUW Tu-142 x4", self.enemyInventory.tu142, nil, nil},
        -- {"RED ASUW Su-24M x2", self.enemyInventory.su24m, "RED A2A Su-27 RUS x2", self.enemyInventory.Su27},
    }

    self.enemyPackageCompositions.fighter = {
        {"RED A2A MiG-21Bis ABK x2",        self.enemyInventory.mig21   },
        {"RED A2A MiG-29S RUS x2",          self.enemyInventory.mig29   },
        {"RED A2A Su-27 RUS x2",            self.enemyInventory.su27    },
    }

    --- }}}4

    -- Zones -- {{{4
    self.zoneNames = {}
    self.zoneNames.landInterdictionZones = {
        "RED Land Interdiction Zone 1",
        "RED Land Interdiction Zone 2",
        "RED Land Interdiction Zone 3",
        "RED Land Interdiction Zone 4",
        "RED Land Interdiction Zone 5",
        "RED Land Interdiction Zone 6",
    }
    self.zoneNames.landInterdictionSpawnZones = {
        "RED Land Attack Spawn Zone 1",
        "RED Land Attack Spawn Zone 2",
        "RED Land Attack Spawn Zone 3",
        "RED Land Attack Spawn Zone 4",
        "RED Land Attack Spawn Zone 5",
        "RED Land Attack Spawn Zone 6",
    }
    self.zoneNames.restrictedZones = {
        "ROEX 1",
        "ROEX 2",
        "ROEX 3",
        "ROEX 4",
        "ROEX 5",
        "ROEX 6",
        "ROEX 7",
        "ROEX 8",
    }
    -- }}}4

    -- Clients -- {{{4
    self.clientUnitNames = {
        "Player 01",
        "Player 02",
        "Player 03",
        "Player 04",
        "Player 05",
        "Player 06",
        "Player 07",
        "Player 08",
        "Player 09",
        "Player 10",
    }
    -- }}}4

    -- Armageddon -- {{{4
    self.isArmageddonStarted = false
    self.armageddonFnId = nil
    self.isRestrictedZoneViolated = false
    self.restrictedZoneCheckFnId = nil
    -- }}}4

    return self
end
-- }}}2

-- Support -- {{{2

function cobralake.ScenarioRunner:startPerpetualAsset(params) -- {{{3
    local assetGroupName = params["assetGroupName"]
    local maxUnitsAliveAtAnyTime = params["maxUnitsAliveAtAnyTime"] or 1
    local maxGroupsSpawnedInTotal = params["maxGroupsSpawnedInTotal"] or 0
    local callsignNameIndex = params["callsignNameIndex"]
    local callsignNumber = params["callsignNumber"] or 1
    local assetSpawner = SPAWN:New(assetGroupName)
        :InitLimit( maxUnitsAliveAtAnyTime, maxGroupsSpawnedInTotal ):SpawnScheduled( 60, 0 )
        :InitRepeatOnEngineShutDown()
        :OnSpawnGroup(function(spawnGroup)
            if callsignNameIndex ~= nil then
                spawnGroup:CommandSetCallsign( callsignNameIndex, callsignNumber, 0)
            end
        end)
    return asset
end
--}}}3

function cobralake.ScenarioRunner:getEnemyAlias(debugAlias) -- {{{3
    self.enemySpawnAliasIndex = self.enemySpawnAliasIndex + 1
    local opAlias = nil
    if cobralake.DEBUG_MODE and debugAlias ~= nil then
        opAlias = string.format("EY%04d [%s]", self.enemySpawnAliasIndex, debugAlias)
    else
        opAlias = string.format("EY%04d", self.enemySpawnAliasIndex)
    end
    return opAlias
end
--}}}3

function cobralake.ScenarioRunner:buildInventories(defs) -- {{{3
    local inv = {}
    for k, v in pairs(defs) do
        inv[k] = cobralake.InventoryTracker{
            name=k,
            itemType=k,
            allocation=v or 0,
        }
    end
    return inv
end
-- }}}3
-- }}}2

-- Armageddon -- {{{2

function cobralake.ScenarioRunner:startRestrictedZoneChecks() -- {{{3
    self:stopRestrictedZoneChecks()
    self.isRestrictedZoneViolated = false
    self.restrictedZones = {}
    for _, zn in pairs(self.zoneNames.restrictedZones) do
        self.restrictedZones[#self.restrictedZones+1] = ZONE:New(zn)
    end
    self.restrictedZoneCheckFnId = timer.scheduleFunction(
        function()
            if self.isRestrictedZoneViolated then
                return nil
            end
            for _, zn in pairs(self.restrictedZones) do
                for _, unitName in pairs(self.clientUnitNames) do
                    local unit = UNIT:FindByName(unitName)
                    if unit ~= nil then
                        if zn:IsVec3InZone(unit:GetVec3()) then
                            self.isRestrictedZoneViolated = true
                            return nil
                        end
                    end
                end
            end
            return timer.getTime() + 60
        end,
        nil,
        timer.getTime() + 60
    )
end
-- }}}3

function cobralake.ScenarioRunner:stopRestrictedZoneChecks() -- {{{3
    if self.restrictedZoneCheckFnId ~= nil then
        timer.removeFunction(self.restrictedZoneCheckFnId)
        self.restrictedZoneCheckFnId = nil
    end
end
-- }}}3

function cobralake.ScenarioRunner:startArmageddonChecks() -- {{{3
    self:stopArmageddonChecks()
    self.armageddonCheckFnId = timer.scheduleFunction(
        function()
            if self.isArmageddonStarted then
                return nil
            end
            if self.isRestrictedZoneViolated then
                if math.random() <= armageddonSettings.zoneViolationTriggerProbability then
                    self:startArmageddon("Restriction Zone Violation")
                    return nil
                end
            end
            return timer.getTime() + 120
        end,
        nil,
        timer.getTime() + 120)
end
-- }}}3

function cobralake.ScenarioRunner:stopArmageddonChecks() -- {{{3
    if self.armageddonCheckFnId ~= nil then
        timer.removeFunction(self.armageddonCheckFnId)
        self.armageddonCheckFnId = nil
    end
end
-- }}}3

function cobralake.ScenarioRunner:startArmageddon(logMessage) -- {{{3
    if logMessage == nil then
        logMessage = ""
    end
    if self.isArmageddonStarted then
        _cobralake_log(string.format("Not running Armageddon started by '%s', because it is already started.", logMessage))
        return
    end
    _cobralake_log(string.format("Running Armageddon scenario started by: '%s'", logMessage))
    local armageddonStartTime = timer.getTime() + math.random(self.scenarioParams.armageddonTriggerDelayRange[1], self.scenarioParams.armageddonTriggerDelayRange[2])
    timer.scheduleFunction(self.launchArmageddonAttack, self, armageddonStartTime)
end
-- }}}3

function cobralake.ScenarioRunner:launchArmageddonAttack() -- {{{3
    if self.isArmageddonStarted then
        return
    end
    self:pauseEnemyMissionLaunches()
    self.enemyAntiCarrierStrikePopUpMissionScheduler:resumeLaunches()
    self.enemyAntiCarrierStrikePopUpMissionScheduler:launchAll(1) -- TODO: make this conditional on # players
    self.enemyAntiCarrierStrikePopUpMissionScheduler:pauseLaunches()
    self.enemyAntiCarrierStrikeMissionScheduler:resumeLaunches()
    self.enemyAntiCarrierStrikeMissionScheduler:launchAll(1) -- TODO: make this conditional on # players
    self.isArmageddonStarted = true
    timer.scheduleFunction(self.detectArmageddon, self,
        timer.getTime() + math.random(self.scenarioParams.armageddonDetectionDelayRange[1], self.scenarioParams.armageddonDetectionDelayRange[2]))
end
-- }}}3

function cobralake.ScenarioRunner:detectArmageddon(delay) -- {{{3
    for i=1,3 do
        cobralake.utils.sendMessageToCoalition(coalition.side.BLUE, "!!!! WHISKEY KILO FOXTROT !!!!")
    end
    self.blueCarrierOps.capControl:sendMessage("All fighters to carrier defense stations!")
    self:scrubCapStationMissions("CONDOR", true, false)
    timer.scheduleFunction(self.respondToArmageddon, self,
        timer.getTime() + math.random(self.scenarioParams.armageddonResponseDelayRange[1], self.scenarioParams.armageddonResponseDelayRange[2]))
end
-- }}}3

function cobralake.ScenarioRunner:respondToArmageddon(delay) -- {{{3
    self.blueCarrierOps.capControl:clearMissionQueue()
    for _, capStationName in pairs({"DURANGO", "DALLAS", "DIEGO"}) do
        self:scrubCapStationMissions(capStationName, false, false)
    end
    for i=1, 2 do -- for multiple round-robin assignment is needed
        for _, capStationName in pairs({"DURANGO", "DALLAS", "DIEGO"}) do
            self.blueCarrierOps.capControl:addMissionToQueue(capStationName, true)
        end
    end
end
-- }}}3

function cobralake.ScenarioRunner:resetArmageddon() -- {{{3
    self.enemyAntiCarrierStrikeMissionScheduler:pauseLaunches()
    self:resetRestrictedZoneChecks()
    self.isArmageddonStarted = false
end
-- }}}3

function cobralake.ScenarioRunner:armageddonBackgroundProbability() -- {{{3
    local t = timer.getTime() / 3600
    if t <= 0.25 then
        return 0.05
    elseif t <= 0.5 then
        return 0.10
    elseif t <= 0.75 then
        return 0.15
    elseif t <= 1.0 then
        return 0.20
    elseif t <= 1.25 then
        return 0.25
    elseif t <= 1.5 then
        return 0.50
    elseif t <= 1.75 then
        return 0.75
    else
        return 0.95
    end
end
-- }}}3

-- }}}2

-- Targets {{{2

function cobralake.ScenarioRunner:runTargets() -- {{{3
    self:setupTargets()
    self:startTargets()
end
-- }}}3

function cobralake.ScenarioRunner:setupTargets(primaryAssetType) -- {{{3
    params = {}
    params.alliedCoalitionId = coalition.side.BLUE
    params.startingSuccessPercentage = 60

    params.groundRuns = {} -- {{{4
    for idx1, groupName in pairs({
            "BLUE RES Trucking 1",
            "BLUE RES Trucking 2",
        }) do
        for idx2, destName in pairs({
                "BLUE RES Heliport Zugdidi 1",
                -- "BLUE RES Station 2",
                -- "BLUE RES Station 3",
                -- "BLUE RES Station 4",
            }) do
            local runConfig = {
                groupName = groupName,
                groupAlias = string.format("%s/%s/%s", groupName, idx1, idx2),
                groupDesc = "trucking",
                accountCategory = "road delivery",
                maxUnitsAliveAtAnyTime = 4,
                maxGroupsSpawnedInTotal = 0,
                spawnFrequency = 120, -- with the units limit above, basically checks for need to respawn max of 4 every 2 mins
                isInitialSpawnDelay = true,
                initialSpawnMin = 0,
                initialSpawnMax = 0,
                -- if 20 units arrive safely, game is "won"
                -- (barring losses of stockpiles)
                perUnitSuccessReward = 4,
                spawnZoneNames = {
                    "BLUE RES Road Spawn Zone 1",
                    "BLUE RES Road Spawn Zone 2",
                    "BLUE RES Road Spawn Zone 3",
                    "BLUE RES Road Spawn Zone 4",
                },
                destStaticName = destName,
            }
            params.groundRuns[#params.groundRuns+1] = runConfig
        end
    end
    -- }}}4

    params.airRuns = {}

    for idx1, originName in pairs({ -- {{{4
            "BLUE RES Heliport Batumi 1",
            "BLUE RES Heliport Batumi 2",
            "BLUE RES Heliport Batumi 3",
            "BLUE RES Heliport Batumi 4",
            -- "BLUE RES Heliport Batumi 5",
            -- "BLUE RES Heliport Batumi 6",
        }) do
        for idx2, destName in pairs({
                "BLUE RES Heliport Zugdidi 1",
                -- "BLUE RES Station 2",
                -- "BLUE RES Station 3",
                -- "BLUE RES Station 4",
            }) do
            groupName = "BLUE RES Transport Helo 1"
            local runConfig = {
                groupName = groupName,
                groupAlias = string.format("%s/%s/%s", groupName, idx1, idx2),
                groupDesc = "helos",
                accountCategory = "air delivery",
                maxUnitsAliveAtAnyTime = 1,
                maxGroupsSpawnedInTotal = 0,
                spawnFrequency = math.random(60, 300),
                isInitialSpawnDelay = true,
                initialSpawnMin = 0,
                initialSpawnMax = 0,
                perUnitSuccessReward = 1,
                spawnAirbaseName = originName,
                spawnAirbaseTakeoff = SPAWN.Takeoff.Hot,
                destAirbaseName = destName,
            }
            params.airRuns[#params.airRuns+1] = runConfig
        end
    end
    -- }}}4

    params.airRuns[#params.airRuns+1] = { -- (German C-17) {{{4
        groupName = "BLUE RES Air Transport 1",
        groupDesc = "heavy air transport",
        accountCategory = "air stockpile",
        maxUnitsAliveAtAnyTime = 1,
        maxGroupsSpawnedInTotal = 0,
        spawnFrequency = math.random(60, 300),
        isInitialSpawnDelay = false,
        initialSpawnMin = 0,
        initialSpawnMax = 0,
        perUnitSuccessReward = 0,
        perUnitLossPenalty = 8,
        spawnZoneNames = {
            "Long Range Transport Air Spawn Zone 1",
        },
        destAirbaseName = AIRBASE.Caucasus.Tbilisi_Lochini,
    }
    -- }}}4

    -- params.airRuns[#params.airRuns+1] = { -- (French C-130 from Tblisi to Senaki) {{{4
    --     groupName = "BLUE RES Air Transport 2",
    --     groupDesc = "medium air transport",
    --     accountCategory = "air stockpile",
    --     maxUnitsAliveAtAnyTime = 1,
    --     maxGroupsSpawnedInTotal = 0,
    --     spawnFrequency = 300,
    --     isInitialSpawnDelay = true,
    --     initialSpawnMin = 0,
    --     initialSpawnMax = 0,
    --     perUnitSuccessReward = 0,
    --     perUnitLossPenalty = 4,
    --     spawnAirbaseName = AIRBASE.Caucasus.Tbilisi_Lochini,
    --     spawnAirbaseTakeoff = SPAWN.Takeoff.Hot,
    --     destAirbaseName = AIRBASE.Caucasus.Senaki_Kolkhi,
    -- }
    -- -- }}}4

    -- params.airRuns[#params.airRuns+1] = { -- (French C-130 from Batumi to Tblisi) {{{4
    --     groupName = "BLUE RES Air Transport 2",
    --     groupDesc = "medium air transport",
    --     accountCategory = "air stockpile",
    --     maxUnitsAliveAtAnyTime = 1,
    --     maxGroupsSpawnedInTotal = 0,
    --     spawnFrequency = 120,
    --     isInitialSpawnDelay = true,
    --     initialSpawnMin = 0,
    --     initialSpawnMax = 0,
    --     perUnitSuccessReward = 0,
    --     perUnitLossPenalty = 4,
    --     spawnAirbaseName = AIRBASE.Caucasus.Batumi,
    --     spawnAirbaseTakeoff = SPAWN.Takeoff.Hot,
    --     destAirbaseName = AIRBASE.Caucasus.Tbilisi_Lochini,
    -- }
    -- -- }}}4

    params.airRuns[#params.airRuns+1] = { -- (Georgian An-26B from Tblisi to Senaki) {{{4
        groupName = "BLUE RES Air Transport 3",
        groupDesc = "medium air transport",
        accountCategory = "air stockpile",
        maxUnitsAliveAtAnyTime = 2,
        maxGroupsSpawnedInTotal = 0,
        spawnFrequency = 120,
        isInitialSpawnDelay = false,
        initialSpawnMin = 0,
        initialSpawnMax = 0,
        perUnitSuccessReward = 0,
        perUnitLossPenalty = 2,
        spawnAirbaseName = AIRBASE.Caucasus.Tbilisi_Lochini,
        spawnAirbaseTakeoff = SPAWN.Takeoff.Hot,
        destAirbaseName = AIRBASE.Caucasus.Senaki_Kolkhi,
    }
    -- }}}4

    -- params.airRuns[#params.airRuns+1] = { -- (Georgian An-26B from Senaki to Tblisi) {{{4
    --     groupName = "BLUE RES Air Transport 3",
    --     -- groupAlias = "BLUE RES Air Transport Reverse Route", -- let CapitalManager take care of alias
    --     groupDesc = "medium air transport",
    --     accountCategory = "air stockpile",
    --     maxUnitsAliveAtAnyTime = 2,
    --     maxGroupsSpawnedInTotal = 0,
    --     spawnFrequency = 180,
    --     isInitialSpawnDelay = true,
    --     initialSpawnMin = 0,
    --     initialSpawnMax = 0,
    --     perUnitSuccessReward = 0,
    --     perUnitLossPenalty = 2,
    --     spawnAirbaseName = AIRBASE.Caucasus.Senaki_Kolkhi,
    --     spawnAirbaseTakeoff = SPAWN.Takeoff.Hot,
    --     destAirbaseName = AIRBASE.Caucasus.Tbilisi_Lochini,
    -- }
    -- -- }}}4

    params.airRuns[#params.airRuns+1] = { -- (Georgian An-26B from Batumi to Kutaisi) {{{4
        groupName = "BLUE RES Air Transport 3",
        groupDesc = "medium air transport",
        accountCategory = "air stockpile",
        maxUnitsAliveAtAnyTime = 2,
        maxGroupsSpawnedInTotal = 0,
        spawnFrequency = 180,
        isInitialSpawnDelay = true,
        initialSpawnMin = 0,
        initialSpawnMax = 0,
        perUnitSuccessReward = 0,
        perUnitLossPenalty = 2,
        spawnAirbaseName = AIRBASE.Caucasus.Batumi,
        spawnAirbaseTakeoff = SPAWN.Takeoff.Hot,
        destAirbaseName = AIRBASE.Caucasus.Kutaisi,
    }
    -- }}}4

    self.capitalManager = cobralake.CapitalManager(params)
    return self.capitalManager
end -- }}}3

function cobralake.ScenarioRunner:startTargets(debugAlias) -- {{{3
    self.capitalManager:start()
end
--- }}}3

-- }}}2

-- Allied Carrier {{{2

function cobralake.ScenarioRunner:runCarrierOps() -- {{{3
    self:setupCarrierOps()
    self:startCarrierOps()
end
--}}}3

function cobralake.ScenarioRunner:setupCarrierOps()  -- {{{3
    self.blueCarrierOps = cobralake.CarrierOps{
        carrierUnit=self.alliedCarrierUnit,
        coalitionId=coalition.side.BLUE,
        tankerGroupName=self.recoveryTankerGroupName,
        capControl={
            assetGroupName=self.alliedCarrierCapGroupName,
            assetGroupAlias=string.format("%s Controlled CAP", self.alliedCarrierUnitName),
            assetGroupInventoryTracker=self.alliedInventory.f14,
            isCapStartTakeoff=nil, -- if nil: spawn in air; otherwise SPAWN.TakeoffRunway etc.
            isCapSpawnInPatrolZone=true, -- if false or nil: spawn in air near carrier
            isCapSpawnAfterClearingTurn=false, -- if false, spawing in patrol zone
        },
        capStationDefinitions=self.alliedCarrierCapStationDefinitions,
    }
end
--}}}3

function cobralake.ScenarioRunner:startCarrierOps()  -- {{{3
    self.blueCarrierOps:start()
end
--}}}3

function cobralake.ScenarioRunner:runAlliedPerpetualAssets() -- {{{3
    for _, ppa in pairs(self.alliedPerpetualAssetGroupNames) do
        self:startPerpetualAsset(ppa)
    end
end
--}}}3

-- }}}2

-- Enemy Missions {{{2

function cobralake.ScenarioRunner:runEnemyMissions() -- {{{3
    self:scheduleEnemyMissions()
    self:startEnemyMissions()
end
--}}}3

function cobralake.ScenarioRunner:startEnemyMissions() -- {{{3
    for _, scheduler in pairs(self.enemyMissionSchedulers) do
        scheduler:start()
    end
end
--}}}3

function cobralake.ScenarioRunner:pauseEnemyMissionLaunches() -- {{{3
    for _, scheduler in pairs(self.enemyMissionSchedulers) do
        scheduler:pauseLaunches()
    end
end
--}}}3

function cobralake.ScenarioRunner:scheduleEnemyMissions() -- {{{3
    self.enemyMissionSchedulers = {}
    local initialTimeRange = self.scenarioParams.interdictionInitialTimeRange
    local waitingTimeRange = self.scenarioParams.interdictionWaitingTimeRange
    self.enemyInterdictionMissionsScheduler = cobralake.CountDrivenMissionScheduler{
            name="Interdiction Mission Scheduler",
            alliedCoalitionId=coalition.side.RED,
            enemyCoalitionId=coalition.side.BLUE,
            enemyAlertMessageDelayRange={1,1},
            checkFrequency=2*60,
            schedulerMaxConcurrentMissions=self.scenarioParams.maxConcurrentEnemyInterdictionMissions,
            waitingTimeFn = function(ms)
                    local wt = 1
                    if ms.numMissionsGenerated == 0 then
                        -- start launching with the specified delay
                        wt = math.random(initialTimeRange[1], initialTimeRange[2])
                    elseif ms.numMissionsGenerated < self.scenarioParams.maxConcurrentEnemyInterdictionMissions then
                        -- launch first batch close to each other
                        wt = math.random(initialTimeRange[1], initialTimeRange[2])
                    else
                        -- replace lost groups with regular waiting time
                        wt = math.random(waitingTimeRange[1], waitingTimeRange[2])
                    end
                    return wt
                end,
            -- schedulerMaxConcurrentMissions=3,
    }
    self.enemyMissionSchedulers[#self.enemyMissionSchedulers+1] = self.enemyInterdictionMissionsScheduler
    self:scheduleEnemyHeloGroundAttackMissions(self.enemyInterdictionMissionsScheduler)
    self:scheduleEnemyGroundAttackStrikeMissions(self.enemyInterdictionMissionsScheduler)
    self:scheduleEnemyOverlandInterdictionMissions(self.enemyInterdictionMissionsScheduler, self.enemyPackageCompositions.groundAttack, nil)
    self:scheduleEnemyOverlandInterdictionMissions(self.enemyInterdictionMissionsScheduler, self.enemyPackageCompositions.airAttack, nil)
    self:scheduleEnemyAntiCarrierStrikeMissions()
end
--}}}3

function cobralake.ScenarioRunner:scheduleEnemyHeloGroundAttackMissions(ms) -- {{{3
    for _, missionPackage in pairs(self.enemyPackageCompositions.heloGroundAttack) do
        local mparams = {
            primaryGroupName=missionPackage[1],
            primaryGroupInventoryTracker=missionPackage[2],
            setInitialAltitude=304,
            patrolAltitudeRange={500, 1000},
        }
        mparams.primaryGroupAlias = self:getEnemyAlias("Helo Ground Attack")
        -- mparams.spawnZoneNames = {interdictionZoneName}
        -- mparams.spawnAirbaseName = AIRBASE.Caucasus.Sukhumi_Babushara
        mparams.spawnAirbaseName = missionPackage[3]
        mparams.spawnAirbaseTakeoff = SPAWN.Takeoff.Hot
        mparams.patrolZoneName = missionPackage[4]
        mparams.engageZoneName = missionPackage[4]
        local mg = cobralake.InterdictionMissionGenerator(mparams)
        ms:addMissionGenerator(mg)
    end
end
--}}}3

function cobralake.ScenarioRunner:scheduleEnemyOverlandInterdictionMissions(ms, packageType, maxActiveMissionsPerGenerator) -- {{{3
    -- for _, packageType in pairs({self.enemyPackageCompositions.groundAttack, self.enemyPackageCompositions.airAttack}) do
        for _, missionPackage in pairs(packageType) do
            local commonParams = {
                primaryGroupName=missionPackage[1],
                primaryGroupInventoryTracker=missionPackage[2],
                escortGroupName= missionPackage[3],
                escortGroupInventory=missionPackage[4],
                patrolAltitudeRange={UTILS.FeetToMeters(10000), UTILS.FeetToMeters(15000)},
                maxActiveMissions=maxActiveMissionsPerGenerator,
            }
            for zidx, interdictionZoneName in pairs(self.zoneNames.landInterdictionZones) do

                local cparams2 = cobralake.utils.table.shallowCopy(commonParams)
                cparams2.patrolZoneName = interdictionZoneName
                cparams2.engageZoneName = interdictionZoneName

                local takeOffParams                = cobralake.utils.table.shallowCopy(cparams2)
                takeOffParams.spawnAirbaseName     = missionPackage[5]
                takeOffParams.spawnAirbaseTakeoff  = SPAWN.Takeoff.Hot
                takeOffParams.primaryGroupAlias    = self:getEnemyAlias("Interdiction TO")
                takeOffParams.escortGroupAlias     = self:getEnemyAlias("Interdiction TO Escort")
                local mg = cobralake.InterdictionMissionGenerator(takeOffParams)
                ms:addMissionGenerator(mg)

                -- local popUpParams             = cobralake.utils.table.shallowCopy(cparams2)
                -- popUpParams.spawnZoneNames    = self.zoneNames.landInterdictionSpawnZones
                -- popUpParams.primaryGroupAlias = self:getEnemyAlias("Interdiction PU")
                -- popUpParams.escortGroupAlias  = self:getEnemyAlias("Interdiction PU Escort")
                -- popUpParams.isDespawnOnRtb    = true
                -- local mg2 = cobralake.InterdictionMissionGenerator(popUpParams)
                -- ms:addMissionGenerator(mg2)

            end
        end
    -- end
end
--}}}3

function cobralake.ScenarioRunner:scheduleEnemyGroundAttackStrikeMissions(ms) -- {{{3
    for idx, missionPackage in pairs(self.enemyPackageCompositions.groundAttack) do
        local commonParams = {
            primaryGroupName=missionPackage[1],
            primaryGroupAlias = self:getEnemyAlias("Ground Attack Strike"),
            primaryGroupInventoryTracker=missionPackage[2],
            -- setInitialAltitude=304,
        }
        local mparams = cobralake.utils.table.shallowCopy(commonParams)
        mparams.escortGroupName      = missionPackage[3]
        mparams.escortGroupAlias     = self:getEnemyAlias("Ground Attack Strike Escort")
        mparams.escortGroupInventory = missionPackage[4]
        mparams.spawnAirbaseName     = missionPackage[5]
        mparams.spawnAirbaseTakeoff  = SPAWN.Takeoff.Hot
        mparams.marshalZoneNames     = missionPackage[6][1]
        mparams.fenceZoneNames       = missionPackage[6][2]
        mparams.ingressZoneNames     = missionPackage[6][3]
        mparams.engageZoneNames      = missionPackage[6][4]
        mparams.egressZoneNames      = missionPackage[6][5]
        mparams.rtbZoneNames         = missionPackage[6][6]
        mparams.landingAirbaseName   = missionPackage[5]
        mparams.targetGroupFn = function()
            return self.capitalManager:getGroundCapitalGroupTarget()
        end
        mparams.movingEngageZoneRadius = 50000
        mparams.transitAltitude = UTILS.FeetToMeters(25000)
        mparams.tacticalAltitude = UTILS.FeetToMeters(5000)
        local mg = cobralake.StrikeMissionGenerator(mparams)
        ms:addMissionGenerator(mg)
    end
end
--}}}3

function cobralake.ScenarioRunner:scheduleEnemyAntiCarrierStrikeMissions() -- {{{3
    -- first wave of Armageddon
    self.enemyAntiCarrierStrikePopUpMissionScheduler = cobralake.TimeDrivenMissionScheduler{
        name="Enemy AntiCarrier Strike TakeOff",
        initialTimeRange=self.scenarioParams.antiCarrierStrikeInitialTimeRange,
        waitingTimeRange=self.scenarioParams.antiCarrierStrikeWaitingTimeRange,
        alliedCoalitionId=coalition.side.RED,
        enemyCoalitionId=coalition.side.BLUE,
        enemyAlertMessageDelayRange=self.scenarioParams.enemyMissionAlertMessageDelayRange,
    }
    -- second wave and subsequent wave of Armageddon
    self.enemyAntiCarrierStrikeMissionScheduler = cobralake.TimeDrivenMissionScheduler{
        name="Enemy AntiCarrier Strike TakeOff",
        initialTimeRange=self.scenarioParams.antiCarrierStrikeInitialTimeRange,
        waitingTimeRange=self.scenarioParams.antiCarrierStrikeWaitingTimeRange,
        alliedCoalitionId=coalition.side.RED,
        enemyCoalitionId=coalition.side.BLUE,
        enemyAlertMessageDelayRange=self.scenarioParams.enemyMissionAlertMessageDelayRange,
    }
    for idx1, asset in pairs(self.enemyAntiCarrier.assets) do
        local commonParams = {
            targetGroup=self.alliedCarrierGroup,
            primaryGroupName=asset[1],
            primaryGroupInventoryTracker=asset[2],
            transitAltitude=10000,
            tacticalAltitude=11000,
        }
        for idx2, routing in pairs(self.enemyAntiCarrier.routes) do
            local mparams = cobralake.utils.table.shallowCopy(commonParams)
            mparams.fenceZoneNames       = routing.routeZones[2]
            mparams.ingressZoneNames     = routing.routeZones[3]
            mparams.engageZoneNames      = routing.routeZones[4]
            mparams.egressZoneNames      = routing.routeZones[5]
            mparams.rtbZoneNames         = routing.routeZones[6]
            mparams.landingAirbaseName   = routing.airbaseName
            for idx3, escortGroupName in pairs({nil, "RED A2A Su-27 RUS x2"}) do
                mparams2 = cobralake.utils.table.shallowCopy(mparams)
                mparams2.escortGroupName      = escortGroupName
                mparams2.escortGroupInventoryTracker = self.enemyInventory.su27

                local takeOffParams = cobralake.utils.table.shallowCopy(mparams2)
                takeOffParams.primaryGroupAlias    = self:getEnemyAlias("AntiCarrier Strike")
                takeOffParams.escortGroupAlias     = self:getEnemyAlias("AntiCarrier Strike Escort")
                takeOffParams.spawnAirbaseName     = routing.airbaseName
                takeOffParams.spawnAirbaseTakeoff  = SPAWN.Takeoff.Hot
                takeOffParams.marshalZoneNames     = routing.routeZones[1]
                local takeOffGenerator = cobralake.StrikeMissionGenerator(takeOffParams)
                self.enemyAntiCarrierStrikeMissionScheduler:addMissionGenerator(takeOffGenerator)

                local popUpParams = cobralake.utils.table.shallowCopy(mparams2)
                popUpParams.primaryGroupAlias    = self:getEnemyAlias("AntiCarrier PopUp Strike")
                popUpParams.escortGroupAlias     = self:getEnemyAlias("AntiCarrier PopUp Strike Escort")
                popUpParams.spawnZoneNames       = routing.routeZones[1] -- spawn in marshal zone
                local popUpGenerator = cobralake.StrikeMissionGenerator(popUpParams)
                self.enemyAntiCarrierStrikePopUpMissionScheduler:addMissionGenerator(popUpGenerator)
            end
        end
    end
    self.enemyAntiCarrierStrikeMissionScheduler:pauseLaunches() -- until Armageddon
    self.enemyAntiCarrierStrikePopUpMissionScheduler:pauseLaunches() -- until Armageddon
    self.enemyMissionSchedulers[1+#self.enemyMissionSchedulers] = self.enemyAntiCarrierStrikeMissionScheduler
end
-- }}}3

function cobralake.ScenarioRunner:launchCarrierAttack(pulseSize, isNoAlert) -- {{{3
    self.enemyAntiCarrierStrikePopUpMissionScheduler:resumeLaunches()
    self.enemyAntiCarrierStrikePopUpMissionScheduler:launchAll(pulseSize, isNoAlert)
    self.enemyAntiCarrierStrikePopUpMissionScheduler:pauseLaunches()
end
--- }}}3

function cobralake.ScenarioRunner:launchEnemyInterdictionMission(pulseSize, isNoAlert) -- {{{3
    for idx=1, pulseSize do
        self.enemyInterdictionMissionsScheduler:launchMission(isNoAlert)
    end
end
--- }}}3

-- }}}2

-- Menu Setups -- {{{2

function cobralake.ScenarioRunner:setupMenus() -- {{{3
    missionCommands.addCommandForCoalition(coalition.side.BLUE, "Mission Status", nil, self.reportScores, self)
    missionCommands.addCommandForCoalition(coalition.side.BLUE, "Asset Status", nil, self.reportAssetStatus, self)
    self:setupMenuMissionCommand()
    -- if self.blueCarrierOps ~= nil then
    --     local carrierDeckSubmenu = missionCommands.addSubMenuForCoalition(coalition.side.BLUE, "Carrier Deck", nil)
    --     self.blueCarrierOps.deckManager:setupMenuCommands(coalition.side.BLUE, carrierDeckSubmenu)
    -- end

    local enemyControlSubmenu = missionCommands.addSubMenuForCoalition(coalition.side.BLUE, "Enemy", nil)
    missionCommands.addCommandForCoalition(coalition.side.BLUE, "Launch Mission", enemyControlSubmenu, self.launchEnemyInterdictionMission, self, 1)
    missionCommands.addCommandForCoalition(coalition.side.BLUE, "Launch Surge", enemyControlSubmenu, self.launchEnemyInterdictionMission, self, self.scenarioParams.maxConcurrentEnemyInterdictionMissions)
    missionCommands.addCommandForCoalition(coalition.side.BLUE, "Launch Carrier Attack", enemyControlSubmenu, self.launchCarrierAttack, self, 1)
    missionCommands.addCommandForCoalition(coalition.side.BLUE, "Launch Carrier Surge Attack", enemyControlSubmenu, self.launchCarrierAttack, self, self.scenarioParams.maxConcurrentEnemyInterdictionMissions)
    missionCommands.addCommandForCoalition(coalition.side.BLUE, "Launch Armageddon", enemyControlSubmenu, self.setArmageddonMode, self, true)
    -- local armageddonSubmenu = missionCommands.addSubMenuForCoalition(coalition.side.BLUE, "Armageddon Mode", enemyControlSubmenu)
    -- missionCommands.addCommandForCoalition(coalition.side.BLUE, "Start", armageddonSubmenu, self.setArmageddonMode, self, true)
    -- missionCommands.addCommandForCoalition(coalition.side.BLUE, "Reset", armageddonSubmenu, self.setArmageddonMode, self, false)

    if cobralake.DEBUG_MODE then
        self:setupMenuEnemyStatus()
    end

end
--- }}}3

function cobralake.ScenarioRunner:setupMenuEnemyStatus() -- {{{3
    local enemySubmenu = missionCommands.addSubMenuForCoalition(coalition.side.BLUE, "Enemy Status", nil)
    missionCommands.addCommandForCoalition(coalition.side.BLUE, "Enemy Inventories", enemySubmenu, self.reportEnemyInventories, self)
    missionCommands.addCommandForCoalition(coalition.side.BLUE, "Enemy Active Missions", enemySubmenu,
        function()
            local s = "ACTIVE ENEMY MISSIONS:"
            local activeMissionGroups = self.enemyInterdictionMissionsScheduler:pollActiveMissions()
            for i=1,#activeMissionGroups do
                local group = activeMissionGroups[i]
                local name = "nil"
                if group and group ~= nil then
                    name = activeMissionGroups[i].GroupName
                end
                s = s .. string.format("\n- %02d/%02d: %s", i, #activeMissionGroups, name)
            end
            cobralake.utils.sendMessageToCoalition(coalition.side.BLUE, s)
        end
    )
end
--- }}}3

function cobralake.ScenarioRunner:setupMenuMissionCommand() -- {{{3
    local capSubmenuId = missionCommands.addSubMenuForCoalition(coalition.side.BLUE, "Mission Command", nil)
    for _, capStationName in pairs({"CONDOR", "DURANGO", "DALLAS", "DIEGO"}) do
        local capStationSubmenuId = missionCommands.addSubMenuForCoalition(coalition.side.BLUE, capStationName, capSubmenuId)
        missionCommands.addCommandForCoalition(
                coalition.side.BLUE,
                string.format("Launch repeating", capStationName),
                capStationSubmenuId,
                function()
                    self.blueCarrierOps.capControl:addMissionToQueue(capStationName, true)
                    self.blueCarrierOps.capControl:sendMessage(string.format("Repeating %s mission added to queue!", capStationName))
                end
        )
        missionCommands.addCommandForCoalition(
                coalition.side.BLUE,
                string.format("Launch non-repeating", capStationName),
                capStationSubmenuId,
                function()
                    self.blueCarrierOps.capControl:addMissionToQueue(capStationName, false)
                    self.blueCarrierOps.capControl:sendMessage(string.format("Single %s mission added to queue!", capStationName))
                end
        )
        missionCommands.addCommandForCoalition(
                coalition.side.BLUE,
                string.format("Scrub all", capStationName),
                capStationSubmenuId,
                function()
                    self:scrubCapStationMissions(capStationName)
                end
        )
    end
    missionCommands.addCommandForCoalition(
            coalition.side.BLUE,
            "Scrub all",
            capSubmenuId,
            function()
                for _, capStationName in pairs({"CONDOR", "DURANGO", "DALLAS", "DIEGO"}) do
                    self:scrubCapStationMissions(capStationName)
                end
            end
    )
    missionCommands.addCommandForCoalition(
            coalition.side.BLUE,
            "Inventory",
            capSubmenuId,
            function()
                local s = "ALLIED INVENTORIES:"
                s = s .. self:composeInventoryReport(self.alliedInventory)
                cobralake.utils.sendMessageToCoalition(coalition.side.BLUE, s, 20)
            end
    )
end
--- }}}3

function cobralake.ScenarioRunner:scrubCapStationMissions(capStationName, isAnnounceScrubbed, isAnnounceNoScrubbed) -- {{{3
    local result = self.blueCarrierOps.capControl:scrubCapStationMissions(capStationName, true)
    local numRemoved = result.numRemovedFromQueue + result.numRemovedFromStation
    if numRemoved > 0 then
        if isAnnounceScrubbed == nil or isAnnounceScrubbed then
            self.blueCarrierOps.capControl:sendMessage(string.format("%s %s missions scrubbed (%s in queue, and %s on station)",
                numRemoved,
                capStationName,
                result.numRemovedFromQueue,
                result.numRemovedFromStation
                ))
        end
    else
        if isAnnounceNoScrubbed == nil or isAnnounceNoScrubbed then
            self.blueCarrierOps.capControl:sendMessage(string.format("No %s missions active or in the queue!", capStationName))
        end
    end
end
--- }}}3

function cobralake.ScenarioRunner:reportScores() -- {{{3
    local s = self.capitalManager:composeScoreboard()
    cobralake.utils.sendMessageToCoalition(coalition.side.BLUE, s, 20, false, false)
end
---}}}3

function cobralake.ScenarioRunner:reportEnemyInventories() -- {{{3
    local s = "ENEMY INVENTORIES:"
    s = s .. self:composeInventoryReport(self.enemyInventory)
    cobralake.utils.sendMessageToCoalition(coalition.side.BLUE, s, 20, false, false)
end
---}}}3

function cobralake.ScenarioRunner:composeInventoryReport(inventory) -- {{{3
    local s = ""
    for key, inv in cobralake.utils.table.pairsBySortedKeys(inventory) do
        s = s .. string.format("\n- %s:\n    %s available of %s remaining allocation (depletions = %s)",
            inv.name,
            inv.available,
            inv.allocation,
            inv.numDepletions)
    end
    return s
end
---}}}3

function cobralake.ScenarioRunner:reportAssetStatus() -- {{{3
    local s = ""
    s = s .. "ALLIED LOSSES:"
    for key, inv in cobralake.utils.table.pairsBySortedKeys(self.alliedInventory) do
        s = s .. string.format("\n- %s: %s (%s available of %s remaining)",
            inv.name,
            inv.numDepletions,
            inv.available,
            inv.allocation
            )
    end
    s = s .. "\n\n(CLAIMED) ENEMY LOSSES:"
    for key, inv in cobralake.utils.table.pairsBySortedKeys(self.enemyInventory) do
        if inv.numDepletions > 0 then
            s = s .. string.format("\n- %s: %s",
                inv.name,
                inv.numDepletions)
        end
    end
    cobralake.utils.sendMessageToCoalition(coalition.side.BLUE, s, 20, false, false)
end
---}}}3

function cobralake.ScenarioRunner:getEscort(groupName) -- {{{3
    if groupName == nil then
        return
    end
    local primaryGroup = GROUP:FindByName(groupName)
    if primaryGroup == nil then
        return
    end
    local escortGroupSpawner = SPAWN:NewWithAlias(self.alliedCarrierCapGroupName, string.format("%s Escort", groupName))
    local escortGroup = nil
    if primaryGroup:InAir() then -- in air
        local interdictionPointVec3 = primaryGroup:GetPointVec3()
        local vec3 = interdictionPointVec3:AddX( 1000 ):AddY(200):AddZ( 400 ):GetVec3()
        escortGroup = escortGroupSpawner:SpawnFromVec3(vec3)
    else
        local spawnAirbase = AIRBASE:FindByName(self.alliedCarrierUnitName)
        escortGroup = escortGroupSpawner:SpawnAtAirbase(spawnAirbase, SPAWN.Takeoff.Hot)
    end
    if escortGroup ~= nil then
        local pv3 = POINT_VEC3:New(-1000, 200, -1000)
        local task = escortGroup:TaskFollow(primaryGroup, pv3:GetVec3())
        escortGroup:SetTask(task, 1)
        escortGroup:CommandSetFrequency(313.050, radio.modulation.AM)
    end
end
--- }}}3

function cobralake.ScenarioRunner:setArmageddonMode(yes) -- {{{3
    if yes then
        self:startArmageddon("Menu Option")
    else
        self:resetArmageddon()
    end
end
--- }}}3

-- }}}2

-- Client Setups -- {{{2
function cobralake.ScenarioRunner:scheduleClientSetups() -- {{{3
    for _, unitName in pairs(self.clientUnitNames) do
        local fid = timer.scheduleFunction(
                function()
                    local unit = UNIT:FindByName(unitName)
                    if unit ~= nil then
                        -- local unitId = unit:getID()
                        self:setupClientUnit(unit)
                        return nil
                    else
                        return timer.getTime() + 10
                    end
                end, {}, timer.getTime() + 10)
    end
end
-- }}}3

function cobralake.ScenarioRunner:setupClientUnit(unit) -- {{{3
    local scenarioRunner = self
    unit:HandleEvent(EVENTS.Land)
    function unit:OnEventLand(EventData)
        if math.random() <= armageddonSettings.onReturnToCarrierProbability then
            timer.scheduleFunction(
                function()
                    scenarioRunner:startArmageddon("On Landing")
                end,
                {}, timer.getTime() + math.random(8 * 60, 16 * 60))
        end
    end
end
-- }}}3

-- }}}2

-- Final {{{2
function cobralake.ScenarioRunner:run()
    -- -- Commerce
    self:runTargets()
    -- -- E3 AWACS, KC-135/130 Tankers, etc.
    self:runAlliedPerpetualAssets()
    -- -- Carrier Ops setup
    self:runCarrierOps()
    -- -- Run attacks
    self:runEnemyMissions()
    -- -- menu setup
    self:setupMenus()
    -- -- client setup
    self:scheduleClientSetups()
    -- -- armageddon
    self:startRestrictedZoneChecks()
    self:startArmageddonChecks()
    if armageddonSettings.backgroundRate > 0 then
        local waitingTime = math.floor(cobralake.utils.exponentialRandomVariable(armageddonSettings.backgroundRate))
        timer.scheduleFunction(
            function()
                if math.random() <= self:armageddonBackgroundProbability() then
                    self:startArmageddon("Background Rate")
                    return nil
                else
                    local nextArmageddonWaitingTime = math.floor(cobralake.utils.exponentialRandomVariable(armageddonSettings.backgroundRate))
                    local t = timer.getTime() + nextArmageddonWaitingTime
                    return t
                end
            end,
            {},
            timer.getTime() + waitingTime)
    end
    -- start the action silently in the background
    self:launchEnemyInterdictionMission(self.scenarioParams.maxConcurrentEnemyInterdictionMissions, true)
end
-- }}}2

-- }}}1

-- MAIN  {{{1
-----------------------------------------------------------------------------------

scenarioRunner = cobralake.ScenarioRunner()
scenarioRunner:run()

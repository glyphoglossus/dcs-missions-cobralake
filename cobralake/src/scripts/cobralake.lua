-- Header {{{1
----------------------------------------------------------------------------------
--  COBRALAKE
--
--  A MOOSE-driven library for a DCS mission featuring:
--
--  *   Randomized adversary interdiction missions against merchant shipping
--      +   Interdiction patrols launched at random times
--      +   Interdiction patrols consist of random aircraft
--  *   Randomized adversary attacks against the carrier group
--      +   Attacks come at random times
--      +   Attacks come from random directions, speeds, and altitudes
--      +   Attacking aircraft type and routes are randomized
--      +   Attack mission profiles modeled with realistic take-off, marshal,
--          fence/push points, ingress points, egress points, etc.
--      +   Attacking aircraft are escorted by random numbers/types of fighters
--      +   Attacking aircraft have to fly the entire mission from base to target and back
--      +   In addition, special "pop up" attacks may come within the carrier
--          outer defense screen
--  *   Target merchant/commerce shipping randomly generated
--  *   Organic as well as external AWACS and air-to-air refueling support for
--      carrier operations
--  *   Basic CAP automatically provided.
--      You can reenforce CAP, intercepting anti-carrier raids, or focus on
--      intercepting antishipping interdiction patrols. Either way, your success
--      will depending on you depleting the enemy resources sufficiently to degrade
--      their capacity to generate sorties (just makes sure you have a carrier
--      to operate from by focusing on defending it if the strikes get too heavy!).
--  *   Enemy sortie capacity restricted to limited number of assets. As enemy
--      assets get attrited due to CAP (including your actions!), air defenses, etc. then
--      their ability to generate sorties (raids on shipping or the carrier).
--      deteriorates until they cannot attack anymore = you WIN!
--  *   Intelligence reports (say, from satellites, ELINT, or HUMINT assets)
--      will alert you regarding raids / interdiction patrols, etc. as they form up,
--      though the accuracy and precision of these will vary randomly (and
--      possibly greatly).
--
--  *   As usual, load MOOSE first then this script. To be sure this happens,
--      use time-sensitive triggers. E.g.,
--      +   MOOSE gets loaded on MISSION START or ONCE with TIME MORE (1) as a condition
--      +   Then this script gets loaded ONCE with TIME MORE (3) as a
--          condition.

----------------------------------------------------------------------------------
-- }}}1

-- DEBUG {{{1
----------------------------------------------------------------------------------
___debugRunID = "2020-06-10 001"
function _cobralake_log (s)
    BASE:I(string.format("[-COBRALAKE/DEBUG-][%s] %s", ___debugRunID, s or "nil"))
end
function _cobralake_debug (s)
    BASE:I(string.format("[-COBRALAKE/DEBUG-][%s] [DEBUG]: %s", ___debugRunID, s or "nil"))
end
function _cobralake_warn (s)
    BASE:I(string.format("[-COBRALAKE/DEBUG-][%s] [WARNING]: %s", ___debugRunID, s or "nil"))
end
function _assertNotNil(obj, fieldname)
    if obj[fieldname] == nil then
        local s = string.format("[-COBRALAKE-] ERROR: '%s' is nil", fieldname)
        error(s)
    end
end
-- }}}1

-- Namespace Definition {{{1
----------------------------------------------------------------------------------

cobralake = {}
-- }}}1

-- Namespace Globals {{{1
----------------------------------------------------------------------------------
cobralake.DEBUG_MODE = false
cobralake.globalAliasIndex = 0
-- }}}1

-- Data {{{1
----------------------------------------------------------------------------------

cobralake.data = {}

cobralake.data.callsignNames = {
    aircraft = {
        [1] = "Enfield",
        [2] = "Springfield",
        [3] = "Uzi",
        [4] = "Colt",
        [5] = "Dodge",
        [6] = "Ford",
        [7] = "Chevy",
        [8] = "Pontiac",
    },
    a10 = {
        [9] = "Hawg",
        [10] = "Boar",
        [11] = "Pig",
        [12] = "Tusk",
    },
    awacs = {
        [1] = "Overlord",
        [2] = "Magic",
        [3] = "Wizard",
        [4] = "Focus",
        [5] = "Darkstar",
    },
    tanker = {
        [1] = "Texaco",
        [2] = "Arco",
        [3] = "Shell",
    },
    jtac = {
        [1] = "Axeman",
        [2] = "Darknight",
        [3] = "Warrior",
        [4] = "Pointer",
        [5] = "Eyeball",
        [6] = "Moonbeam",
        [7] = "Whiplash",
        [8] = "Finger",
        [9] = "Pinpoint",
        [10] = "Ferret",
        [11] = "Shaba",
        [12] = "Playboy",
        [13] = "Hammer",
        [14] = "Jaguar",
        [15] = "Deathstar",
        [16] = "Anvil",
        [17] = "Firefly",
        [18] = "Mantis",
        [19] = "Badger",
    },
}

-- }}}1

-- Utility Functions {{{1
----------------------------------------------------------------------------------

cobralake.utils = {}

cobralake.utils.table = { -- {{{2

-- Table Utility Functions
-- From:
--  table.lua
--  Additions to Lua's built-in table functions.
--  https://github.com/premake/premake-4.x/blob/master/src/base/table.lua
--  Copyright (c) 2002-2008 Jason Perkins and the Premake project
--  Distributed under the terms of the BSD License, see LICENSE.txt

    -- iterate over table by key order
    pairsBySortedKeys = function(t, f)
      local a = {}
      for n in pairs(t) do table.insert(a, n) end
      table.sort(a, f)
      local i = 0      -- iterator variable
      local iter = function ()   -- iterator function
        i = i + 1
        if a[i] == nil then return nil
        else return a[i], t[a[i]]
        end
      end
      return iter
    end,

    -- Returns true if the table contains the specified value.
	contains = function(t, value)
		for _,v in pairs(t) do
			if (v == value) then
				return true
			end
		end
		return false
	end,

    -- Enumerates an array of objects and returns a new table containing
    -- only the value of one particular field.
	extract = function(arr, fname)
		local result = { }
		for _,v in ipairs(arr) do
			table.insert(result, v[fname])
		end
		return result
	end,

    -- Flattens a hierarchy of tables into a single array containing all
    -- of the values.
	flatten = function(arr)
		local result = { }

		local function flatten(arr)
			for _, v in ipairs(arr) do
				if type(v) == "table" then
					flatten(v)
				else
					table.insert(result, v)
				end
			end
		end

		flatten(arr)
		return result
	end,

    -- Merges an array of items into a string.
	implode = function(arr, before, after, between)
		local result = ""
		for _,v in ipairs(arr) do
			if (result ~= "" and between) then
				result = result .. between
			end
			result = result .. before .. v .. after
		end
		return result
	end,


    -- Inserts a value of array of values into a table. If the value is
    -- itself a table, its contents are enumerated and added instead. So
    -- these inputs give these outputs:
    --   "x" -> { "x" }
    --   { "x", "y" } -> { "x", "y" }
    --   { "x", { "y" }} -> { "x", "y" }
	insertflat = function(tbl, values)
		if type(values) == "table" then
			for _, value in ipairs(values) do
				table.insertflat(tbl, value)
			end
		else
			table.insert(tbl, values)
		end
	end,

    -- Returns true if the table is empty, and contains no indexed or keyed values.
	isempty = function(t)
		return next(t) == nil
	end,

    -- Adds the values from one array to the end of another and
    -- returns the result.
	join = function(...)
		local result = { }
		for _,t in ipairs({...}) do
			if type(t) == "table" then
				for _,v in ipairs(t) do
					table.insert(result, v)
				end
			else
				table.insert(result, t)
			end
		end
		return result
	end,

    -- Return a list of all keys used in a table.
	keys = function(tbl)
		local keys = {}
		for k, _ in pairs(tbl) do
			table.insert(keys, k)
		end
		return keys
	end,

    -- Adds the key-value associations from one table into another
    -- and returns the resulting merged table.
	merge = function(...)
		local result = { }
		for _,t in ipairs({...}) do
			if type(t) == "table" then
				for k,v in pairs(t) do
					result[k] = v
				end
			else
				error("invalid value")
			end
		end
		return result
	end,

    -- Translates the values contained in array, using the specified
    -- translation table, and returns the results in a new array.
	translate = function(arr, translation)
		local result = { }
		for _, value in ipairs(arr) do
			local tvalue
			if type(translation) == "function" then
				tvalue = translation(value)
			else
				tvalue = translation[value]
			end
			if (tvalue) then
				table.insert(result, tvalue)
			end
		end
		return result
	end,

    shallowCopy = function(original)
        local copy = {}
        for key, value in pairs(original) do
            copy[key] = value
        end
        return copy
    end,

} -- cobralake.utils.table
-- }}} 2

function cobralake.utils.exponentialRandomVariable(rate)
    return -1.0 * math.log(1 - math.random())/rate
end

function cobralake.utils.ensureUniqueAlias(name, localSpawnerIndex)
    -- note, need to provide alias different from group name, otherwise things
    -- go zany (in particular, properties do not get shared properly between
    -- different instantions of GROUP around the same (actual group)
    if name == nil then
        return nil
    end
    cobralake.globalAliasIndex = cobralake.globalAliasIndex + 1
    if localSpawnerIndex == nil then
        localSpawnerIndex = cobralake.globalAliasIndex
    end
    return string.format("%s(%s)", name, localSpawnerIndex)
end

function cobralake.utils.countNumAliveGroups(spawnner)
    local group, index = spawnner:GetFirstAliveGroup()
    local count = 0
    while group ~= nil do
        count = count + 1
        group, index = spawnner:GetNextAliveGroup(index)
    end
    return count
end

function cobralake.utils.sendMessageToCoalition(coalitionId, message, duration, cls, isNotPlayRadioSound)
    if duration == nil then
        duration = 10
    end
    if cls == nil then
        cls = false
    end
    if not isNotPlayRadioSound then
        trigger.action.outSoundForCoalition(coalitionId, "messageprefix.ogg")
    end
    trigger.action.outTextForCoalition(coalitionId, message, duration, cls)
    -- _cobralake_log(message)
end

function cobralake.utils.sendDelayedMessageToCoalition(delay, coalitionId, message, duration, cls)
    if delay == nil or delay == 0 then
        cobralake.utils.sendMessageToCoalition(coalitionId, message, duration, cls)
    else
        local fid = timer.scheduleFunction(
                function()
                    cobralake.utils.sendMessageToCoalition(coalitionId, message, duration, cls)
                end, {}, timer.getTime() + delay)
    end
end

function cobralake.utils.roundToNearest(num, nearest)
    local mult = 1/nearest
    return math.floor(num * mult + 0.5) / mult
end

function cobralake.utils.isEmpty(t)
    if t == nil then
        return true
    elseif next(t) == nil then
        return true
    else
        return false
    end
end

function cobralake.utils.zonesFromNames(zoneNames)
    if cobralake.utils.isEmpty(zoneNames) then
        return nil
    end
    zones = {}
    for i = 1, #zoneNames do
        zones[i] = ZONE:New(zoneNames[i])
    end
    return zones
end

function cobralake.utils.parseZonesFromArgs(init, prefix, default)
    local arg1 = prefix .. "Zones"
    local arg2 = prefix .. "ZoneNames"
    local zones = nil
    if init[arg1] ~= nil then
        zones = init[arg1]
    elseif init[arg2] ~= nil then
        zones = cobralake.utils.zonesFromNames(init[arg2])
    else
        zones = default
    end
    return zones
end

function cobralake.utils.metersToNauticalMiles(meters)
    return meters * 0.000539957
end

function cobralake.utils.randomAirWaypointFromZones(params)
    if params.zones == nil or #params.zones == 0 then
        return nil
    end
    local zone = params.zones[math.random(1, #params.zones)]
    local coord = COORDINATE:NewFromVec2(zone:GetRandomVec2()):SetAltitude(params.altitude, true)
    local wp = coord:WaypointAirTurningPoint(params.altitudeType, params.speed, params.tasks)

    ---- FOR DEBUGGING: CREATE MARKS OF WAYPOINT
    -- if cobralake.DEBUGWPCTR == nil then
    --     cobralake.DEBUGWPCTR = 0
    -- end
    -- local Vec3 = coord:GetVec3()
    -- trigger.action.markToAll( cobralake.DEBUGWPCTR, string.format("WP %s", cobralake.DEBUGWPCTR), Vec3, false, "" )
    -- cobralake.DEBUGWPCTR = cobralake.DEBUGWPCTR + 1
    ---- FOR DEBUGGING: CREATE MARKS OF WAYPOINT

    return wp
end

function cobralake.utils.addRandomAirWaypointFromZones(params)
    local wp = cobralake.utils.randomAirWaypointFromZones(params)
    if wp ~= nil then
        params.waypoints[#params.waypoints+1] = wp
    end
    return params.waypoints
end

function cobralake.utils.composeNavGridReference(reportCoordinate, anglePrecisionLevel)
    local desc = ""
    local bullsCoordinate = COORDINATE:NewFromVec3( coalition.getMainRefPoint( coalition.side.BLUE ) )
    local directionVec3 = bullsCoordinate:GetDirectionVec3( reportCoordinate )
    local angleRadians =  reportCoordinate:GetAngleRadians( directionVec3 )
    local angleDegrees = math.deg(angleRadians)
    local angleCall = cobralake.utils.roundToNearest(angleDegrees / 10, 1)
    desc = desc .. tostring(angleCall) .. " "
    local distance = reportCoordinate:Get2DDistance( bullsCoordinate )
    distance = cobralake.utils.metersToNauticalMiles(distance)
    local navGridLetter = "Z"
    if distance <= 50 then
        navGridLetter = "Alpha"
    elseif distance <= 100 then
        navGridLetter = "Bravo"
    elseif distance <= 150 then
        navGridLetter = "Charlie"
    elseif distance <= 200 then
        navGridLetter = "Delta"
    elseif distance <= 250 then
        navGridLetter = "Echo"
    elseif distance <= 300 then
        navGridLetter = "Golf"
    elseif distance <= 350 then
        navGridLetter = "Hotel"
    elseif distance <= 400 then
        navGridLetter = "India"
    elseif distance <= 450 then
        navGridLetter = "Juliet"
    elseif distance <= 500 then
        navGridLetter = "Kilo"
    elseif distance <= 550 then
        navGridLetter = "Lima"
    elseif distance <= 600 then
        navGridLetter = "Mike"
    elseif distance <= 650 then
        navGridLetter = "November"
    elseif distance <= 700 then
        navGridLetter = "Oscar"
    elseif distance <= 750 then
        navGridLetter = "Papa"
    elseif distance <= 800 then
        navGridLetter = "Quebec"
    elseif distance <= 850 then
        navGridLetter = "Romeo"
    elseif distance <= 900 then
        navGridLetter = "Sierra"
    elseif distance <= 950 then
        navGridLetter = "Tango"
    elseif distance <= 1000 then
        navGridLetter = "Uniform"
    elseif distance <= 1050 then
        navGridLetter = "Victor"
    elseif distance <= 1100 then
        navGridLetter = "Whiskey"
    elseif distance <= 1150 then
        navGridLetter = "Xray"
    elseif distance <= 1250 then
        navGridLetter = "Yankee"
    elseif distance <= 1300 then
        navGridLetter = "Zulu"
    end
    desc = desc .. navGridLetter
    return desc
end

function cobralake.utils.composeBullseyeReference(reportCoordinate, anglePrecisionLevel, roundDistanceToNearest)
    local desc = ""
    local bullsCoordinate = COORDINATE:NewFromVec3( coalition.getMainRefPoint( coalition.side.BLUE ) )
    local directionVec3 = bullsCoordinate:GetDirectionVec3( reportCoordinate )
    local angleRadians =  reportCoordinate:GetAngleRadians( directionVec3 )
    local angleDegrees = math.deg(angleRadians)
    if anglePrecisionLevel <= 1 then -- 4-point cardinal direction
        if angleDegrees >= 315 and angleDegrees < 45 then
            desc = desc .. "0" -- "North"
        elseif angleDegrees >= 45 and angleDegrees < 135 then
            desc = desc .. "90" -- "East"
        elseif angleDegrees >= 135 and angleDegrees < 225 then
            desc = desc .. "180" -- "South"
        -- elseif angleDegrees >= 225 and angleDegrees < 315 then
        else
            desc = desc .. "270" -- "West"
        end
    elseif anglePrecisionLevel <= 2 then
        if angleDegrees >= 337.5 and angleDegrees < 22.5 then
            desc = desc .. "0" -- "North"
        elseif angleDegrees >= 22.5 and angleDegrees < 67.5 then
            desc = desc .. "45" -- North East"
        elseif angleDegrees >= 67.5 and angleDegrees < 112.5 then
            desc = desc .. "90" -- "East"
        elseif angleDegrees >= 112.5 and angleDegrees < 157.5 then
            desc = desc .. "135" -- "South East"
        elseif angleDegrees >= 157.5 and angleDegrees < 202.5 then
            desc = desc .. "180" -- "South"
        elseif angleDegrees >= 202.5 and angleDegrees < 247.5 then
            desc = desc .. "225" -- "South West"
        elseif angleDegrees >= 247.5 and angleDegrees < 292.5 then
            desc = desc .. "270" -- "West"
        -- elseif angleDegrees >= 292.5 and angleDegrees < 337.5 then
        else
            desc = desc .. "315" -- "North West"
        end
    elseif anglePrecisionLevel <= 3 then
        desc = desc .. tostring(cobralake.utils.roundToNearest(angleDegrees, 30))
    elseif anglePrecisionLevel <= 4 then
        desc = desc .. tostring(cobralake.utils.roundToNearest(angleDegrees, 15))
    elseif anglePrecisionLevel <= 5 then
        desc = desc .. tostring(cobralake.utils.roundToNearest(angleDegrees, 10))
    elseif anglePrecisionLevel <= 6 then
        desc = desc .. tostring(cobralake.utils.roundToNearest(angleDegrees, 5))
    end
    desc = desc .. " "
    local distance = reportCoordinate:Get2DDistance( bullsCoordinate )
    distance = cobralake.utils.metersToNauticalMiles(distance)
    if roundDistanceToNearest ~= nil then
        distance = cobralake.utils.roundToNearest(distance, roundDistanceToNearest)
    end
    desc = desc .. "for " .. tostring(distance)
    return desc
end

function cobralake.utils.composeSpatialReference(reportCoordinate, anglePrecisionLevel, roundDistanceToNearest)
    -- return " bulls " .. cobralake.utils.composeBullseyeReference(reportCoordinate, anglePrecisionLevel, roundDistanceToNearest)
    return cobralake.utils.composeNavGridReference(reportCoordinate, anglePrecisionLevel, roundDistanceToNearest)
end

-- Message to be sent when missions launched
function cobralake.utils.composeMissionEnemyAlertMessage(
        numUnits,
        unitTypeDesc,
        unitCoordinate,
        groupDesignation,
        observedBehavior)
    local desc = ""
    local leader = "      "
    -- desc = desc .. "S2: *** " .. missionType .. " ALERT ***\n"
    desc = desc .. "S2: *** RAID ALERT ***\n"
    desc = desc .. leader
    if math.random(0, 1) == 1 then
        local reportNumUnits = math.floor(numUnits * (math.random(200, 4000)/1000))
        if reportNumUnits < 2 then
            desc = desc .. "Single "
        else
            desc = desc .. tostring(reportNumUnits) .. " x "
        end
    else
        desc = desc .. "Multiple "
    end
    if math.random(0, 1) == 1 then
        desc = desc .. unitTypeDesc .. " "
    else
        desc = desc .. "aircraft "
    end
    -- desc = desc .. strike:GetCategory() .. "\n" --- "1"
    -- desc = desc .. strike:GetCategoryName() .. "\n" -- "Airplane"
    -- desc = desc .. strike:IdentifiableName .. "\n"
    local reportCoordinate = unitCoordinate:GetRandomCoordinateInRadius(80000, 50000)
    local bullsCoordinate = COORDINATE:NewFromVec3( coalition.getMainRefPoint( coalition.side.BLUE ) )

    local directionVec3 = bullsCoordinate:GetDirectionVec3( reportCoordinate )
    local angleRadians =  reportCoordinate:GetAngleRadians( directionVec3 )
    -- local angleDegrees = UTILS.Round( UTILS.ToDegree( angleRadians ), 0 )
    local anglePrecisionLevel = math.random(1, 3)
    desc = desc .. observedBehavior
    if math.random() > 0.5 then
        desc = desc .. " at " .. cobralake.utils.composeSpatialReference(reportCoordinate, anglePrecisionLevel, 50)
    end
    -- desc = desc .. "\n"
    -- desc = desc .. leader
    -- desc = desc .. "Designated as: " .. groupDesignation
    return desc
end

function cobralake.utils.ammoAvailability(focalGroup)
    local totalAmmo = 0
    local totalShells = 0
    local totalBombs = 0
    local totalMissiles = 0
    local focalUnits = focalGroup:GetUnits()
    if focalUnits == nil then
        return nil
    end
    for i = 1, #focalUnits do
        local focalUnit = focalGroup:GetUnit( i )
        nammo, nshells, nrockets, nbombs, nmissiles = focalUnit:GetAmmunition()
        totalAmmo = totalAmmo + nammo
        totalShells = totalShells + nshells
        totalBombs = totalBombs + nbombs
        totalMissiles = totalMissiles + nmissiles
    end
    return {
        missiles=totalMissiles,
        bombs=totalBombs,
        shells=totalShells,
        ammo=totalAmmo,
    }
end

function cobralake.utils.getShotCall(weaponName)
    -- below are regex patterns, with "." standing in for "-" to avoid escaping "-" by using "--"
    if weaponName == nil then
        return "engaging"
    elseif weaponName:match("AIM.7") then
        return "Fox 1"
    elseif weaponName:match("AIM.9") then
        return "Fox 2"
    elseif weaponName:match("AIM.120") ~= nil or weaponName:match("AIM.54") ~= nil then  -- other patterns: "AIM--154", but not "AIM-154"
        return "Fox 3"
    else
        return string.format("firing %s", weaponName)
    end
end

-- }}}1

-- cobralake.Deque {{{1
----------------------------------------------------------------------------------
cobralake.Deque = {} --
cobralake.Deque.__index = cobralake.Deque

setmetatable(cobralake.Deque, {
    __call = function (cls, ...)
        return cls.new(...)
    end,
})

function cobralake.Deque:new ()
    local self = setmetatable({}, cobralake.Deque)
    self:clear() -- also creates
    return self
end

function cobralake.Deque:clear()
    self.data = {first = 0, last = -1}
end

function cobralake.Deque:pushLeft(value)
    local first = self.data.first - 1
    self.data.first = first
    self.data[first] = value
end

function cobralake.Deque:pushRight(value)
    local last = self.data.last + 1
    self.data.last = last
    self.data[last] = value
end

function cobralake.Deque:popLeft()
    local first = self.data.first
    if first > self.data.last then error("self.data is empty") end
    local value = self.data[first]
    self.data[first] = nil        -- to allow garbage collection
    self.data.first = first + 1
    return value
end

function cobralake.Deque:popRight()
    local last = self.data.last
    if self.data.first > last then error("self.data is empty") end
    local value = self.data[last]
    self.data[last] = nil         -- to allow garbage collection
    self.data.last = last - 1
    return value
end

function cobralake.Deque:isEmpty()
    return self.data.first > self.data.last
end

-- }}}1

-- cobralake.Queue {{{1
----------------------------------------------------------------------------------

cobralake.Queue = {} -- {{{2
cobralake.Queue.__index = cobralake.Queue
setmetatable(cobralake.Queue, {
    __index=cobralake.Deque,
    __call = function (cls, ...)
    return cls.new(...)
    end,
})
-- }}}2

function cobralake.Queue.new(init) -- {{{2
    local baseclass = cobralake.Deque(init)
    local self = setmetatable(baseclass, cobralake.Queue)
    return self
end
-- }}}2

function cobralake.Queue:push(value) -- {{{2
    self:pushLeft(value)
end
-- }}}2

function cobralake.Queue:pop() -- {{{2
    return self:popRight()
end
-- }}}2

-- }}}1

-- cobralake.InventoryTracker  {{{1
----------------------------------------------------------------------------------

cobralake.InventoryTracker = {}
cobralake.InventoryTracker.__index = cobralake.InventoryTracker

setmetatable(cobralake.InventoryTracker, {
    __call = function (cls, ...)
        return cls.new(...)
    end,
})

function cobralake.InventoryTracker.new(init)
    local self = setmetatable({}, cobralake.InventoryTracker)
    self.name = init["name"]
    self.allocation = init["allocation"]
    self.available = self.allocation
    self.numDepletions = 0
    return self
end

-- checkout: temporarily renove from available pool
-- if ``isHandleEvents`` is true, then event handlers are added to units,
-- to update the inventory as they land (EngineShutdown = return to stock)
-- or die (Crash, Dead = total allocation depleted). Note that this will
-- override any existing event uhandlers for the units, including those
-- added via independent Moose objects wrapping up the same DCS units.
-- Conversely, any event handlers attached to the units after will override
-- these.
function cobralake.InventoryTracker:checkoutGroup(group,
        isHandleEvents,
        deadEventCallback,
        crashEventCallback,
        engineShutdownEventCallback)
    if group == nil then
        return
    end
    local units = group:GetUnits()
    if units == nil or #units == 0 then
        return nil
    end
    if #units > self.available then
        return nil
    end
    for _, unit in pairs(units) do
        unit.inventoryTracker = self
        if isHandleEvents then
            unit:HandleEvent(EVENTS.Dead)
            function unit:OnEventDead(EventData)
                if unit.isDespawned == nil or not unit.isDespawned then
                    unit.inventoryTracker:deplete(1)
                    unit.inventoryTracker:unhandleEvents(unit)
                end
                if deadEventCallback ~= nil then
                    deadEventCallback(group, unit, EventData)
                end
            end
            unit:HandleEvent(EVENTS.Crash)
            function unit:OnEventCrash(EventData)
                if unit.isDespawned == nil or not unit.isDespawned then
                    unit.inventoryTracker:deplete(1)
                    unit.inventoryTracker:unhandleEvents(unit)
                end
                if crashEventCallback ~= nil then
                    crashEventCallback(group, unit, EventData)
                end
            end
            unit:HandleEvent(EVENTS.EngineShutdown)
            function unit:OnEventEngineShutdown(EventData)
                if unit.isDespawned == nil or not unit.isDespawned then
                    unit.inventoryTracker:restore(1)
                    unit.inventoryTracker:unhandleEvents(unit)
                end
                if engineShutdownEventCallback ~= nil then
                    engineShutdownEventCallback(group, unit, EventData)
                end
            end
        end
    end
    return self:checkout(#units)
end

function cobralake.InventoryTracker:unhandleEvents(unit)
    function unit:OnEventDead(EventData)
    end
    function unit:OnEventCrash(EventData)
    end
    function unit:OnEventEngineShutdown(EventData)
    end
    -- unit:ResetEvents() -- does nothing
    -- unit:UnHandleEvent(EVENTS.Dead) -- 39210: attempt to call method 'RemoveForUnit' (a nil value)
    -- unit:UnHandleEvent(EVENTS.Crash)
    -- unit:UnHandleEvent(EVENTS.EngineShutdown)
    -- unit:EventDispatcher():RemoveEvent(unit, EVENTS.Dead) -- 6791: attempt to call method 'F2' (a nil value)
    -- unit:EventDispatcher():RemoveEvent(unit, EVENTS.Crash)
    -- unit:EventDispatcher():RemoveEvent(unit, EVENTS.EngineShutdown)
end

function cobralake.InventoryTracker:checkinAndDespawnGroup(group, isRaiseDestroyEvent)
    if group == nil then
        return
    end
    if isRaiseDestroyEvent == nil then
        isRaiseDestroyEvent = true
    end
    for _, unit in pairs(group:GetUnits()) do
        if unit:IsAlive() then
            unit.inventoryTracker:restore(1)
            unit.isDespawned = true
            unit.inventoryTracker:unhandleEvents(unit)
        end
    end
    group:Destroy(isRaiseDestroyEvent)
end

function cobralake.InventoryTracker:checkout(count)
    if self.available == 0 then
        return 0
    end
    if count == nil then
        count = 1
    end
    self.available = self.available - count
    if self.available < 0 then
        self.available = 0
    end
    return self.available
end

-- checkout: restore to available pool
function cobralake.InventoryTracker:restore(count)
    if count == nil then
        count = 1
    end
    self.available = self.available + count
    if self.available > self.allocation then
        self.available = self.allocation
    end
    return self.available
end

-- checkout: permanently remove from inventory, reducing allocation
function cobralake.InventoryTracker:deplete(count)
    if self.allocation == 0 then
        return 0
    end
    if count == nil then
        count = 1
    end
    self.allocation = self.allocation - count
    if self.allocation < 0 then
        self.allocation = 0
    else
        self.numDepletions = self.numDepletions + 1
    end
    return self.allocation
end

-- check availability
function cobralake.InventoryTracker:hasAvailable(count)
    if count <= self.available then
        return true
    else
        return false
    end
end

function cobralake.InventoryTracker:numCheckedOut()
    local n = self.allocation - self.available
    if n <= 0 then
        n = 0
    end
    return n
end

-- }}}1

-- cobralake.CapitalManager  {{{1
----------------------------------------------------------------------------------

cobralake.CapitalManager = {}
cobralake.CapitalManager.__index = cobralake.CapitalManager

setmetatable(cobralake.CapitalManager, { -- {{{2
    __call = function (cls, ...)
        return cls.new(...)
    end,
})
--}}}2

function cobralake.CapitalManager.new(init) -- {{{2
    local self = setmetatable({}, cobralake.CapitalManager)
    self.alliedCoalitionId = init["alliedCoalitionId"]
    self.currentSuccessPercentage = init["startingSuccessPercentage"] or 0
    self.capitalCategories = {
        "ground",
        "air",
        "sea",
    }
    self.targetLists = {}
    self.transportRunAccounts = {}
    self.stationaryAccounts = {}
    self.runConfigs = {}
    for _, t in pairs(self.capitalCategories) do
        self.runConfigs[t] = init[string.format("%sRuns", t)] or nil
        self.targetLists[t] = {}
    end
    self.spawnnerIndex = 0
    return self
end
-- }}}2

function cobralake.CapitalManager:getAnyTarget() -- {{{2
    local aggregateTargetList = {}
    for _, targetType in pairs(self.targetLists) do
        local targetGroup = self:getCapitalGroupTargetByType(targetType)
        if targetGroup ~= nil then
            aggregateTargetList[#aggregateTargetList + 1] = targetGroup
        end
    end
    if #aggregateTargetList > 0 then
        return aggregateTargetList[math.random(1, #aggregateTargetList)]
    else
        return nil
    end
end
-- }}}2

function cobralake.CapitalManager:getGroundCapitalGroupTarget() -- {{{2
    return self:getCapitalGroupTargetByType("ground")
end
-- }}}2

function cobralake.CapitalManager:getCapitalGroupTargetByType(targetType) -- {{{2
    if #self.targetLists[targetType] == 0 then
        return nil
    end
    local newList = {}
    for _, tt in pairs(self.targetLists[targetType]) do
        if tt and tt ~= nil and tt:IsAlive() and tt.isInTransit then
            newList[#newList + 1] = tt
        end
    end
    self.targetLists[targetType] = newList
    if #self.targetLists[targetType] > 0 then
        return self.targetLists[targetType][math.random(1, #self.targetLists[targetType])]
    else
        return nil
    end
end
-- }}}2

function cobralake.CapitalManager:start() -- {{{2
    self:startGroundRunningCapital()
    self:startAirRunningCapital()
end
-- }}}2

function cobralake.CapitalManager:startGroundRunningCapital() -- {{{2
    self:startRunningCapital("ground",
        function(spawnGroup, runConfig)
            local destCoordinate = runConfig["destCoordinate"]
            if destCoordinate == nil then
                if runConfig["destGroupName"] ~= nil then
                    local destGroup = GROUP:FindByName(runConfig["destGroupName"])
                    if destGroup ~= nil then
                        destCoordinate = destGroup:GetCoordinate()
                    end
                elseif runConfig["destStaticName"] ~= nil then
                    local destStatic = STATIC:FindByName(runConfig["destStaticName"])
                    if destStatic then
                        destCoordinate = destStatic:GetCoordinate()
                    end
                end
            end

            spawnGroup:OptionROTNoReaction() -- to keep moving even when hit
            spawnGroup:OptionAlarmStateGreen() -- ditto
            -- also: uncheck "Disperse Under Fire" option in ME

            if destCoordinate ~= nil then
                -- TODO: support other types of routes, e.g. off road or rail
                spawnGroup:RouteGroundOnRoad(
                    destCoordinate,
                    spawnGroup:GetSpeedMax(),
                    1,
                    "On Road",
                    function(controllable, waypointIndex, numWaypoints, capitalManager)
                        if waypointIndex == numWaypoints then
                            capitalManager:onRunCompletion(controllable, true)
                        end
                    end,
                    {spawnGroup.capitalManager}
                )
            end
        end
    )
end
-- }}}2

function cobralake.CapitalManager:startAirRunningCapital() -- {{{2
    self:startRunningCapital("air",
        function(spawnGroup, runConfig)
            if runConfig["destAirbaseName"] ~= nil then
                local landingAirbase = AIRBASE:FindByName(runConfig["destAirbaseName"])
                if landingAirbase == nil then
                    _cobralake_warn(string.format("Airbase not found: %s", runConfig["destAirbaseName"] or nil))
                    return
                end
                local destWp = landingAirbase:GetCoordinate():SetAltitude(900, true):WaypointAirTurningPoint("RADIO", 150, {})
                local destLandingWp = landingAirbase:GetCoordinate():SetAltitude(300):WaypointAirLanding(UTILS.KnotsToKmph(50), landingAirbase, {}, "Landing")
                -- local destWp = landingAirbase:GetCoordinate():SetAltitude(400, true):WaypointAirTurningPoint()
                -- local destLandingWp = landingAirbase:GetCoordinate():SetAltitude(100):WaypointAirLanding()
                spawnGroup:Route({destWp, destLandingWp}, 1)

                spawnGroup:HandleEvent(EVENTS.Land)
                function spawnGroup:OnEventLand(EventData)
                    local eventIniGroup = EventData.IniGroup --Wrapper.Group#GROUP
                    if eventIniGroup == nil or not eventIniGroup:IsAlive() then
                        return
                    end
                    -- Group name. When spawning it will have #001 attached.
                    local eventIniGroupname = eventIniGroup:GetName()
                    -- Check that we have the right eventIniGroup and that it should be respawned.
                    if eventIniGroupname == spawnGroup:GetName() then
                        local airbase = nil --Wrapper.Airbase#AIRBASE
                        local airbasename = "unknown"
                        if EventData.Place then
                            airbase = EventData.Place
                            airbasename = airbase:GetName()
                            if airbasename == runConfig["destAirbaseName"] then
                                -- complete, but do not terminate yet: check if return trip is needed
                                -- (EngineShutdown event handler will terminate/despawn either way)
                                eventIniGroup.capitalManager:onRunCompletion(eventIniGroup, false)
                            end
                        end
                    end
                end

                -- -- does not work very well; no repeat take-off
                -- spawnGroup:HandleEvent(EVENTS.Land)
                -- function spawnGroup:OnEventLand(EventData)
                --     -- Group that shut down the engine.
                --     local eventIniGroup = EventData.IniGroup --Wrapper.Group#GROUP
                --     -- Check if eventIniGroup is alive.
                --     if eventIniGroup == nil or not eventIniGroup:IsAlive() then
                --         return
                --     end
                --     -- Group name. When spawning it will have #001 attached.
                --     local eventIniGroupname = eventIniGroup:GetName()
                --     -- Check that we have the right eventIniGroup and that it should be respawned.
                --     if eventIniGroupname == spawnGroup:GetName() then
                --         local airbase = nil --Wrapper.Airbase#AIRBASE
                --         local airbasename = "unknown"
                --         if EventData.Place then
                --             airbase = EventData.Place
                --             airbasename = airbase:GetName()
                --             if airbasename == runConfig["destAirbaseName"] then
                --                 -- complete, but do not terminate yet: check if return trip is needed
                --                 -- (EngineShutdown event handler will terminate/despawn either way)
                --                 eventIniGroup.capitalManager:onRunCompletion(eventIniGroup, false)
                --                 if runConfig["spawnAirbaseName"] == nil then
                --                     -- return trip
                --                     local returnAirbase = AIRBASE:FindByName(runConfig["spawnAirbaseName"])
                --                     if returnAirbase ~= nil then
                --                         local takeOffWp = airbase:GetCoordinate():WaypointAirTakeOffParkingHot()
                --                         local returnWp = returnAirbase:GetCoordinate():SetAltitude(400, true):WaypointAirTurningPoint(nil, 150, {})
                --                         local returnLandingWp = returnAirbase:GetCoordinate():SetAltitude(300):WaypointAirLanding(UTILS.KnotsToKmph(50), returnAirbase, {}, "Landing")
                --                         eventIniGroup:Route({takeOffWp, returnWp, returnLandingWp}, 1)
                --                     end
                --                 end
                --             end
                --         end
                --     end
                -- end

            end

            spawnGroup:HandleEvent(EVENTS.EngineShutdown)
            function spawnGroup:OnEventEngineShutdown(EventData)
                -- This gets called even before the the unit crash/dead
                -- handlers when a single-unit group is instantly killed
                -- But NOT when the last surviving unit of a multiple unit
                -- group is killed ...????
                -- We are using it here to trigger a respawn. In the case of
                -- multi-unit groups, the per-unit death processing, if needed,
                -- gets correctly taken care of by unit event handlers before
                -- we get here. But not in the case of single-unit groups. So
                -- we check to see if units need to be processed before terminating.
                -- We use a flag-guard in the crash/dead unit processing to avoid double
                -- processing a unit.
                local eventIniGroup = EventData.IniGroup --- can we use 'spawnGroup' instead? 'self'?
                -- if not eventIniGroup:IsAlive() then
                    local position = cobralake.utils.composeSpatialReference(eventIniGroup:GetCoordinate(), 5, 10)
                    for _,unit in pairs(eventIniGroup:GetUnits()) do
                        if not unit:IsAlive() then
                            eventIniGroup.capitalManager:onCrashOrDeadUnit(unit, position)
                        end
                    end
                -- end
                eventIniGroup.capitalManager:terminateRun(eventIniGroup)
            end

        end
    )
end
-- }}}2

function cobralake.CapitalManager:startRunningCapital(capitalCategory, onSpawnSupplementalFn) -- {{{2
    if self.runConfigs[capitalCategory] == nil then
        return
    end
    for _, runConfig in pairs(self.runConfigs[capitalCategory]) do
        local groupSpawner = self:getGroupSpawner(runConfig)
        groupSpawner:OnSpawnGroup(
            function(spawnGroup)
                spawnGroup.capitalManager = self
                spawnGroup.capitalCategory = capitalCategory
                spawnGroup.accountCategory = runConfig["accountCategory"] or string.format("%s", capitalCategory)
                spawnGroup.groupDesc = runConfig["groupDesc"] or "units"
                spawnGroup.perUnitSuccessReward = runConfig["perUnitSuccessReward"] or 5
                spawnGroup.startTime = timer.getTime()
                spawnGroup.isInTransit = true
                if spawnGroup.capitalManager.transportRunAccounts[spawnGroup.accountCategory] == nil then
                    spawnGroup.capitalManager.transportRunAccounts[spawnGroup.accountCategory] = {
                        spawned = 0,
                        lost = 0,
                        succeeded = 0,
                    }
                end
                spawnGroup.capitalManager.transportRunAccounts[spawnGroup.accountCategory].spawned = spawnGroup.capitalManager.transportRunAccounts[spawnGroup.accountCategory].spawned + spawnGroup:GetInitialSize()
                onSpawnSupplementalFn(spawnGroup, runConfig)
                self:addRunningUnitEventHandlers(spawnGroup, runConfig)
                local tlist = spawnGroup.capitalManager.targetLists[capitalCategory]
                tlist[#tlist+1] = spawnGroup
            end
        )
        self:runGroupSpawner(groupSpawner, runConfig)
    end
end
--- }}}2

function cobralake.CapitalManager:onRunCompletion(spawnGroup, isTerminate) -- {{{2
    spawnGroup.endTime = timer.getTime()
    local numAliveUnits = spawnGroup:CountAliveUnits()
    local supplyContribution = (numAliveUnits * spawnGroup.perUnitSuccessReward)
    local account = spawnGroup.capitalManager.transportRunAccounts[spawnGroup.accountCategory]
    account.succeeded = account.succeeded + numAliveUnits
    spawnGroup.capitalManager.currentSuccessPercentage = spawnGroup.capitalManager.currentSuccessPercentage + supplyContribution
    cobralake.utils.sendMessageToCoalition(
        spawnGroup.capitalManager.alliedCoalitionId,
        string.format(
                "%s: Arrived at destination after travel time of %0.1f hours, with %s of %s units remaining.",
                spawnGroup.GroupName,
                (spawnGroup.endTime - spawnGroup.startTime)/3600,
                numAliveUnits,
                spawnGroup:GetInitialSize()
                ))
    if isTerminate ~= nil and isTerminate then
        self:terminateRun(spawnGroup)
    end
end
-- }}}2

function cobralake.CapitalManager:terminateRun(spawnGroup) -- {{{2
    spawnGroup.isInTransit = false
    spawnGroup:Destroy(true) -- remove group with event; event required to trigger MOOSE-driven respawn
end
-- }}}2

function cobralake.CapitalManager:addRunningUnitEventHandlers(spawnGroup, runConfig) -- {{{2
        local position = cobralake.utils.composeSpatialReference(spawnGroup:GetCoordinate(), 5, 10)
        local units = spawnGroup:GetUnits()
        for i = 1, #units do
            local unit = units[i]
            unit.perUnitLossPenalty = runConfig["perUnitLossPenalty"] or 0
            unit:HandleEvent(EVENTS.Hit)
            function unit:OnEventHit( EventData )
                -- local unit = EventData.IniUnit
                if not unit:GetGroup().isInTransit then
                    return
                end
                local t = timer.getTime()
                if unit._last_hit == nil or t - unit._last_hit > 120 then
                    unit._last_hit = t
                    cobralake.utils.sendMessageToCoalition(
                        spawnGroup.capitalManager.alliedCoalitionId,
                        string.format("S2: Attack on %s reported: %s, at bulls %s",
                        spawnGroup.groupDesc or "units",
                        unit:Name(),
                        position)
                    )
                end
            end
            unit:HandleEvent(EVENTS.Dead)
            function unit:OnEventDead( EventData )
                -- local xunit = EventData.IniUnit
                spawnGroup.capitalManager:onCrashOrDeadUnit(unit, position)
            end
            unit:HandleEvent(EVENTS.Crash)
            function unit:OnEventCrash( EventData )
                -- local xunit = EventData.IniUnit
                spawnGroup.capitalManager:onCrashOrDeadUnit(unit, position)
            end
        end
end
-- }}}2

function cobralake.CapitalManager:onCrashOrDeadUnit(unit, position) -- {{{2
    local spawnGroup = unit:GetGroup()
    if not spawnGroup.isInTransit then
        unit.isDone = true
        return
    end
    if unit.isDone ~= nil and unit.isDone then
        return
    end
    unit.isDone = true
    local acc = spawnGroup.capitalManager.transportRunAccounts[spawnGroup.accountCategory]
    acc.lost = acc.lost + 1
    spawnGroup.capitalManager.currentSuccessPercentage = spawnGroup.capitalManager.currentSuccessPercentage - unit.perUnitLossPenalty
    cobralake.utils.sendMessageToCoalition(
        spawnGroup.capitalManager.alliedCoalitionId,
        -- string.format("S2: Loss of %s reported: %s, at bulls %s",
        --     spawnGroup.groupDesc or "units",
        --     unit:Name(),
        --     position)
        string.format("S2: Loss of %s reported: %s, at %s",
            spawnGroup.groupDesc or "units",
            unit:Name(),
            position)
    )
end
-- }}}2

function cobralake.CapitalManager:runGroupSpawner(groupSpawner, runConfig) -- {{{2
    local spawnFrequency = runConfig["spawnFrequency"] or nil
    if spawnFrequency ~= nil then
        groupSpawner:SpawnScheduled(spawnFrequency, 0.5 )
    end
    local initialSpawnMin = runConfig["initialSpawnMin"] or 1
    local initialSpawnMax = runConfig["initialSpawnMax"] or 1
    if initialSpawnMax > 0 then
        for i=1, math.random(initialSpawnMin, initialSpawnMax) do
            groupSpawner:Spawn()
        end
    end
end
-- }}}2

function cobralake.CapitalManager:getGroupSpawner(runConfig) -- {{{2
    self.spawnnerIndex = self.spawnnerIndex + 1
    local groupName = runConfig["groupName"]
    local groupAlias = runConfig["groupAlias"] or cobralake.utils.ensureUniqueAlias(groupName, self.spawnnerIndex)
    local groupSpawner = SPAWN:NewWithAlias(groupName, groupAlias)
    groupSpawner:InitCleanUp(300)
    if runConfig["isInitialSpawnDelay"] then
        groupSpawner:InitDelayOn()
    end
    local maxUnitsAliveAtAnyTime = runConfig["maxUnitsAliveAtAnyTime"] or 4
    local maxGroupsSpawnedInTotal = runConfig["maxGroupsSpawnedInTotal"] or nil
    if maxUnitsAliveAtAnyTime ~= nil then
        groupSpawner:InitLimit(maxUnitsAliveAtAnyTime, maxGroupsSpawnedInTotal)
    end
    if runConfig["spawnAirbaseName"] ~= nil then
        local spawnAirbaseTakeoff = runConfig["spawnAirbaseTakeoff"] or SPAWN.Takeoff.Hot
        groupSpawner:InitAirbase(runConfig["spawnAirbaseName"], spawnAirbaseTakeoff)
    elseif runConfig["spawnZones"] ~= nil or runConfig["spawnZoneNames"] ~= nil then
        local spawnZones = cobralake.utils.parseZonesFromArgs(runConfig, "spawn", nil)
        if spawnZones ~= nil then
            groupSpawner:InitRandomizeZones(spawnZones)
        end
    end
    groupSpawner.capitalManager = self
    return groupSpawner
end
-- }}}2

function cobralake.CapitalManager:composeScoreboard() -- {{{2
    local s = "*** SUPPLY REPORT ***"
    s = s .. "\n\n"
    s = s .. string.format("CURRENT SUPPLY LEVEL: %s%%", self.currentSuccessPercentage)
    s = s .. "\n"
    for key, account in cobralake.utils.table.pairsBySortedKeys(self.transportRunAccounts) do
        if account.spawned > 0 then
            s = s .. "\n"
            s = s .. string.format("> %s RUNS: DISPATCHED = %s, IN TRANSIT = %s, LOST = %s, ARRIVED = %s.",
                key:upper(),
                account.spawned,
                account.spawned - account.lost - account.succeeded,
                account.lost,
                account.succeeded
                )
        end
    end
    return s
end -- }}}2

-- }}}1

-- cobralake.BaseMissionGenerator {{{1
----------------------------------------------------------------------------------

cobralake.BaseMissionGenerator = {} -- {{{2
cobralake.BaseMissionGenerator.__index = cobralake.BaseMissionGenerator
setmetatable(cobralake.BaseMissionGenerator, {
    __call = function (cls, ...)
    return cls.new(...)
    end,
})
-- }}}2

function cobralake.BaseMissionGenerator.new(init) -- {{{2
    local self = setmetatable({}, cobralake.BaseMissionGenerator)
    self.primaryGroupName = init["primaryGroupName"]
    self.primaryGroupAlias = init["primaryGroupAlias"] or cobralake.utils.ensureUniqueAlias(self.primaryGroupName)
    self.primaryGroupInventoryTracker = init["primaryGroupInventoryTracker"] or nil
    self.escortGroupName = init["escortGroupName"]
    -- note, need to provide alias different from group name, otherwise things
    -- go zany (in particular, properties do not get shared properly between
    -- different instantions of GROUP around the same (actual group)
    if self.escortGroupName ~= nil then
        self.escortGroupAlias = init["escortGroupAlias"] or cobralake.utils.ensureUniqueAlias(self.escortGroupName)
    end
    self.escortGroupInventoryTracker = init["escortGroupInventoryTracker"] or nil

    -- Spawn option, in order of priority:
    -- (1) spawning: airbase
    --     spawnAirbase=AIRBASE:FindByName(AIRBASE.Caucasus.Kutaisi)
    --     spawnAirbase=AIRBASE:FindByName(AIRBASE.Caucasus.Mozdok)
    --     spawnAirbase=AIRBASE:FindByName("CVN 27") -- for carrier
    --  or
    --     spawnAirbaseName=AIRBASE.Caucasus.Kutaisi
    --     spawnAirbaseName=AIRBASE.Caucasus.Mozdok
    --     spawnAirbaseName="CVN 27" -- for carrier
    self.spawnAirbase = init["spawnAirbase"] or nil
    if self.spawnAirbase == nil and init["spawnAirbaseName"] ~= nil then
        self.spawnAirbase = AIRBASE:FindByName(init["spawnAirbaseName"])
    end
    --     spawnAirbaseTakeoff=SPAWN.Takeoff.Hot
    --     spawnAirbaseTakeoff=SPAWN.Takeoff.Cold
    self.spawnAirbaseTakeoff = init["spawnAirbaseTakeoff"] or SPAWN.Takeoff.Hot
    -- (2) spawning: a list of possible zones
    self.spawnZones = cobralake.utils.parseZonesFromArgs(init, "spawn", nil)
    -- (3) spawn at ME unit settings (``:Spawn()``)

    -- (in the case of random air spawns): override starting waypoint to go tactical altitude / speed on spawn
    self.setInitialAltitude = init["setInitialAltitude"] or nil

    self.probabilityOfAvailability = init["probabilityOfAvailability"] or 1.0
    self.totalMissionGenerationLimit = init["totalMissionGenerationLimit"] or nil
    self.maxActiveMissions = init["maxActiveMissions"] or nil

    self.numMissionsGenerated = 0
    self.primaryGroupSpawner = SPAWN:NewWithAlias(self.primaryGroupName, self.primaryGroupAlias)
    self.primaryGroupSpawner:InitCleanUp(180)
    self.primaryGroupSpawner:OnSpawnGroup(
        function(spawnGroup)
            self:onSpawnMainGroup(spawnGroup)
        end)
    do
        local gtest = GROUP:FindByName(self.primaryGroupName)
        self.primaryGroupSize = gtest:GetInitialSize()
        self.primaryGroupMaxSpeed = gtest:GetSpeedMax()
    end
    if self.escortGroupName ~= nil then
        self.escortGroupSpawner = SPAWN:NewWithAlias(self.escortGroupName, self.escortGroupAlias)
        self.escortGroupSpawner:InitCleanUp(180)
        self.escortGroupSpawner:OnSpawnGroup(
            function(spawnGroup)
                self:onSpawnEscortGroup(spawnGroup)
            end)
    else
        self.escortGroupSpawner =  nil
    end
    if self.escortGroupName ~= nil then
        self.escortGroupSize = GROUP:FindByName(self.escortGroupName):GetInitialSize()
    else
        self.escortGroupSize = 0
    end

    return self
end
-- }}}2

function cobralake.BaseMissionGenerator:generate() -- {{{2
    local status = nil
    primaryGroup, escortGroup = self:spawnAssets()
    if primaryGroup ~= nil then
        status = self:setupMission(primaryGroup, escortGroup)
        self.numMissionsGenerated = self.numMissionsGenerated + 1
    end
    result = {
        status=status,
        primaryGroup=primaryGroup,
        escortGroup=escortGroup,
    }
    return result
end
-- }}}2

function cobralake.BaseMissionGenerator:setupMission(primaryGroup, escortGroup) -- {{{2
    return true
end
-- }}}2

function cobralake.BaseMissionGenerator:spawnAssets() -- {{{2
    local primaryGroup = nil
    local escortGroup = nil
    -- self.primaryGroupSpawner:InitCleanUp(300)
    if self.primaryGroupInventoryTracker ~= nil then
        if not self.primaryGroupInventoryTracker:hasAvailable(self.primaryGroupSize) then
            return nil, nil
        end
    end
    if self.spawnAirbase ~= nil then
        primaryGroup = self.primaryGroupSpawner:SpawnAtAirbase(self.spawnAirbase, self.spawnAirbaseTakeoff)
    elseif self.spawnZones ~= nil then
        -- primaryGroup = self.primaryGroupSpawner:InitRandomizeZones(self.spawnZones):Spawn()
        if self.setInitialAltitude ~= nil then
            local spawnZone = self.spawnZones[math.random(1, #self.spawnZones)]
            local spawnPointVec2 = spawnZone:GetRandomPointVec2()
            -- local spawnPointVec3 = POINT_VEC3:NewFromVec2(spawnPointVec2, spawnPointVec2:GetLandHeight() + self.setInitialAltitude)
            primaryGroup = self.primaryGroupSpawner:SpawnFromPointVec2(spawnPointVec2, spawnPointVec2:GetLandHeight() + self.setInitialAltitude, spawnPointVec2:GetLandHeight() + self.setInitialAltitude)
        else
            primaryGroup = self.primaryGroupSpawner:InitRandomizeZones(self.spawnZones):Spawn()
        end
    else
        primaryGroup = self.primaryGroupSpawner:Spawn()
    end
    if primaryGroup == nil then
        return nil, nil
    end
    self:checkoutAndPrepareGroup(primaryGroup, self.primaryGroupInventoryTracker)

    if self.escortGroupSpawner ~= nil then
        -- self.escortGroupSpawner:InitCleanUp(300)
        if self.escortGroupInventoryTracker ~= nil then
            if not self.escortGroupInventoryTracker:hasAvailable(self.escortGroupSize) then
                return nil, nil
            end
        end
        if self.spawnAirbase ~= nil then
            escortGroup = self.escortGroupSpawner:SpawnAtAirbase(self.spawnAirbase, self.spawnAirbaseTakeoff)
        elseif self.spawnZones ~= nil then
            -- interdiction was spawned randomly
            -- Here we make sure the escort spawns
            -- close to the interdiction group
            local interdictionPointVec3 = primaryGroup:GetPointVec3()
            local vec3 = interdictionPointVec3:AddX( 1000 ):AddY(200):AddZ( 400 ):GetVec3()
            escortGroup = self.escortGroupSpawner:SpawnFromVec3(vec3)
        else
            -- non-randomized spawning
            escortGroup = self.escortGroupSpawner:Spawn()
        end
        if escortGroup == nil then
            return nil, nil
        end
        self:checkoutAndPrepareGroup(escortGroup, self.escortGroupInventoryTracker)
        local pv3 = POINT_VEC3:New(-1000, 200, 500)
        local escortTask = escortGroup:TaskEscort(primaryGroup, pv3:GetVec3())
        escortGroup:SetTask(escortTask, 1)
    end
    return primaryGroup, escortGroup
end
-- }}}2

function cobralake.BaseMissionGenerator:checkoutAndPrepareGroup(group, groupInventoryTracker) -- {{{2
    group.missionGenerator = self
    group.groupInventoryTracker = groupInventoryTracker
    if groupInventoryTracker ~= nil then
        groupInventoryTracker:checkoutGroup(group,
            true,
            nil,
            function(group, unit, EventData) -- OnCrash
                if unit._shotByUnit ~= nil then
                    local group = unit._shotByUnit:GetGroup()
                    local capControl = group.capControl
                    if capControl ~= nil then
                        capControl:sendMessageFromUnit(unit._shotByUnit, "Splash one!")
                    end
                end
            end,
            nil)
    end
    self:setupPatrolGroupEventHandlers(group)
end
-- }}}2

function cobralake.BaseMissionGenerator:setupPatrolGroupEventHandlers(group) -- {{{2
    for _, unit in pairs(group:GetUnits()) do
        unit:HandleEvent(EVENTS.Hit)
        function unit:OnEventHit( EventData )
            -- EventData.IniUnit is the unit making the shot
            -- EventData.TgtUnit is the unit that was hit
            -- Target = EventData.TgtUnit
            -- TgtName = EventData.TgtUnitName
            unit._shotByUnit = EventData.IniUnit
        end
  -- EventData.IniUnit is the unit making the shot
  -- EventData.TgtUnit is the unit that was hit
    end
end
--}}}2

function cobralake.BaseMissionGenerator:onSpawnMainGroup(group) -- {{{2
end
-- }}}2

function cobralake.BaseMissionGenerator:onSpawnEscortGroup(group) -- {{{2
end
-- }}}2

function cobralake.BaseMissionGenerator:isInventorySufficientForMission() -- {{{2
    if self.primaryGroupInventoryTracker ~= nil or self.escortGroupInventoryTracker ~= nil then
        if self.primaryGroupInventoryTracker == self.escortGroupInventoryTracker then
            local size = self.primaryGroupSize + self.escortGroupSize
            if not self.primaryGroupInventoryTracker:hasAvailable(size) then
                return false
            end
        else
            if self.primaryGroupInventoryTracker ~= nil then
                if not self.primaryGroupInventoryTracker:hasAvailable(self.primaryGroupSize) then
                    return false
                end
            end
            if self.escortGroupInventoryTracker ~= nil and self.escortGroupSize > 0 then
                if not self.escortGroupInventoryTracker:hasAvailable(self.escortGroupSize) then
                    return false
                end
            end
        end
    end
    return true
end
-- }}}2

function cobralake.BaseMissionGenerator:numActiveMissions() -- {{{2
    return cobralake.utils.countNumAliveGroups(self.primaryGroupSpawner)
end
-- }}}2

function cobralake.BaseMissionGenerator:isMaxActiveMissionLimit() -- {{{2
    if self.maxActiveMissions == nil then
        return false
    end
    if self:numActiveMissions() >= self.maxActiveMissions then
        return true
    end
    return false
end
-- }}}2

function cobralake.BaseMissionGenerator:isOperational() -- {{{2
    if self.totalMissionGenerationLimit ~= nil and self.numMissionsGenerated > self.totalMissionGenerationLimit  then
        return false
    end
    if self:isMaxActiveMissionLimit() then
        return false
    end
    if not self:isInventorySufficientForMission() then
        return false
    end
    if math.random() < self.probabilityOfAvailability then
        return true
    else
        return false
    end
end
-- }}}2

function cobralake.BaseMissionGenerator:setupAiZone(aiZone, primaryGroup) -- {{{2
    aiZone.missionGenerator = self
    aiZone:SetControllable(primaryGroup)
    check, checkScheduleID = SCHEDULER:New(
        nil,
        function()
            local ammo = cobralake.utils.ammoAvailability(primaryGroup)
            if ammo == nil then
                aiZone:__Accomplish(1)
            elseif ammo.missiles + ammo.bombs == 0 then
                aiZone:__Accomplish(1)
            end
        end,
        {}, 20, 60, 0.2 )
    function aiZone:OnAfterRTB(aiGroup)
        if aiGroup.missionGenerator.isDespawnOnRtb then
            aiGroup.groupInventoryTracker:checkinAndDespawnGroup(aiGroup, true)
        end
    end
    function aiZone:OnAfterAccomplish( Controllable, From, Event, To )
        check:Stop( checkScheduleID )
        aiZone:__RTB( 1 )
    end
end
-- }}}2

-- }}}1

-- cobralake.InterdictionMissionGenerator {{{1
----------------------------------------------------------------------------------

cobralake.InterdictionMissionGenerator = {} -- {{{2
cobralake.InterdictionMissionGenerator.__index = cobralake.InterdictionMissionGenerator
setmetatable(cobralake.InterdictionMissionGenerator, {
    __index=cobralake.BaseMissionGenerator,
    __call = function (cls, ...)
    return cls.new(...)
    end,
})
-- }}}2

function cobralake.InterdictionMissionGenerator.new(init) -- {{{2
    local baseclass = cobralake.BaseMissionGenerator(init)
    local self = setmetatable(baseclass, cobralake.InterdictionMissionGenerator)
    self.patrolZone = ZONE:New(init["patrolZoneName"])
    self.engageZone = ZONE:New(init["engageZoneName"])
    self.patrolAltitudeRange = init["patrolAltitudeRange"] or {UTILS.FeetToMeters(12000), UTILS.FeetToMeters(20000)}
    self.isDespawnOnRtb = init["isDespawnOnRtb"]
    if self.isDespawnOnRtb == nil then
        self.isDespawnOnRtb = false
    end
    return self
end
-- }}}2

function cobralake.InterdictionMissionGenerator:setupMission(primaryGroup, escortGroup) -- {{{2
    local aiBaiZone = AI_BAI_ZONE:New(self.patrolZone, self.patrolAltitudeRange[1], self.patrolAltitudeRange[2], UTILS.KnotsToKmph(400), UTILS.KnotsToKmph(600), self.engageZone, "RADIO")
    self:setupAiZone(aiBaiZone, primaryGroup)
    aiBaiZone:SearchOn()
    aiBaiZone:__Start(1)
    aiBaiZone:__Engage(1)
    return true
end
-- }}}2

-- }}}1

-- cobralake.AirToAirPatrolMissionGenerator {{{1
----------------------------------------------------------------------------------

cobralake.AirToAirPatrolMissionGenerator = {} -- {{{2
cobralake.AirToAirPatrolMissionGenerator.__index = cobralake.AirToAirPatrolMissionGenerator
setmetatable(cobralake.AirToAirPatrolMissionGenerator, {
    __index=cobralake.BaseMissionGenerator,
    __call = function (cls, ...)
    return cls.new(...)
    end,
})
-- }}}2

function cobralake.AirToAirPatrolMissionGenerator.new(init) -- {{{2
    local baseclass = cobralake.BaseMissionGenerator(init)
    local self = setmetatable(baseclass, cobralake.AirToAirPatrolMissionGenerator)
    self.patrolZone = ZONE:New(init["patrolZoneName"])
    self.engageZone = ZONE:New(init["engageZoneName"])
    self.engageRange = init["engageRange"] or 100000
    self.patrolAltitudeRange = init["patrolAltitudeRange"] or {UTILS.FeetToMeters(12000), UTILS.FeetToMeters(20000)}
    return self
end
-- }}}2

function cobralake.AirToAirPatrolMissionGenerator:setupMission(primaryGroup, escortGroup) -- {{{2
    local capZone = AI_CAP_ZONE:New(
        self.patrolZone,                -- @param Core.Zone#ZONE_BASE PatrolZone The @{Zone} where the patrol needs to be executed.
        self.patrolAltitudeRange[1], -- @param DCS#Altitude PatrolFloorAltitude The lowest altitude in meters where to execute the patrol.
        self.patrolAltitudeRange[2], -- @param DCS#Altitude PatrolCeilingAltitude The highest altitude in meters where to execute the patrol.
        UTILS.KnotsToKmph(400), -- @param DCS#Speed  PatrolMinSpeed The minimum speed of the @{Wrapper.Controllable} in km/h.
        UTILS.KnotsToKmph(600), -- @param DCS#Speed  PatrolMaxSpeed The maximum speed of the @{Wrapper.Controllable} in km/h.
        "RADIO"-- "BARO" -- @param DCS#AltitudeType PatrolAltType The altitude type ("RADIO"=="AGL", "BARO"=="ASL"). Defaults to RADIO
    )
    if self.engageZone ~= nil then
        self:SetEngageZone(self.engageZone)
    else
        if self.engageRange ~= nil then
            self.engageRange = 200000
        else
            self:SetEngageRange(self.engageRange)
        end
    end
    self:setupAiZone(capZone, primaryGroup)
    capZone:__Start(1)
    return true
end
-- }}}2

-- }}}1

-- cobralake.StrikeMissionGenerator {{{1
----------------------------------------------------------------------------------

cobralake.StrikeMissionGenerator = {} -- {{{2
cobralake.StrikeMissionGenerator.__index = cobralake.StrikeMissionGenerator
setmetatable(cobralake.StrikeMissionGenerator, {
    __index=cobralake.BaseMissionGenerator,
    __call = function (cls, ...)
    return cls.new(...)
    end,
})
-- }}}2

function cobralake.StrikeMissionGenerator.new(init) -- {{{2
    local baseclass = cobralake.BaseMissionGenerator(init)
    local self = setmetatable(baseclass, cobralake.StrikeMissionGenerator)

    -- the strike target group
    self.targetGroup = nil
    if init["targetGroup"] ~= nil then
        self.targetGroup = init["targetGroup"]
    elseif init["targetGroupName"] ~= nil then
        self.targetGroup = GROUP:FindByName(init["targetGroupName"])
    else
        -- A target group can be provided dynamically through a function that
        -- will be called without arguments and should return a MOOSE *group*
        -- to be attacked.
        --  E.g.,
        --     mg = StrikeMissionGenerator{
        --         targetGroupFn=function()
        --             local targetGroup = capitalManager:getGroundCapitalGroup()
        --             return targetGroup
        --         end,
        --         primaryGroupName="RED A2G Su-24M BOMB RUS x2",
        --         primaryGroupAlias="Ground Strike",
        --         primaryGroupInventoryTracker=inventory1,
        --         escortGroupName="RED A2A MiG-29S RUS x2",
        --         escortGroupAlias="Ground Strike Escort",
        --         escortGroupInventoryTracker=inventory2,
        --         spawnAirbaseName=AIRBASE.Caucasus.Kutaisi,
        --         spawnAirbaseTakeoff=SPAWN.Takeoff.Hot,
        --         marshalZoneNames={"RED Marshal Zone 1"},
        --         fenceZoneNames={"RED Land Fence Zone 1"},
        --         ingressZoneNames={"RED Land Ingress Zone 1"},
        --         landingAirbaseName=AIRBASE.Caucasus.Kobuleti,
        --     }
        --     mg:generate()
        self.targetGroupFn = init["targetGroupFn"] or nil
    end

    -- strike routing: each parameter should be passed a list of zones or nil;
    -- for each waypoint, a random zone from the list will be selected and a
    -- random point within the zone will be selected
    -- routing waypoint: marshal area after take-off
    self.marshalZones = cobralake.utils.parseZonesFromArgs(init, "marshal", nil)
    -- routing waypoint: area(s) where flights go tactical
    self.fenceZones = cobralake.utils.parseZonesFromArgs(init, "fence", nil)
    -- routing waypoint: area(s) from which to approach IP
    self.ingressZones = cobralake.utils.parseZonesFromArgs(init, "ingress", nil)
    -- routing waypoint: actual IP/Combat/Attack Zone(s),
    -- NOTE: this is the only waypoint zone that NEEDS to be provided for an
    -- attack to take place. If not provided by client, then a moving zone will
    -- automatically be created around the target group.
    self.engageZones = cobralake.utils.parseZonesFromArgs(init, "engage", nil)
    -- If a moving zone is created on the fly for an engage zone (i.e., instead
    -- of using user-specified engage zone), this is the radius to be used.
    self.movingEngageZoneRadius = init["movingEngageZoneRadius"] or 50000
    -- routing waypoint: area(s) from which to exit the vul zone
    self.egressZones = cobralake.utils.parseZonesFromArgs(init, "egress", nil)
    self.rtbZones = cobralake.utils.parseZonesFromArgs(init, "rtb", nil)
    self.landingAirbase = init["landingAirbase"] or nil
    if self.landingAirbase == nil and init["landingAirbaseName"] ~= nil then
        self.landingAirbase = AIRBASE:FindByName(init["landingAirbaseName"])
    end

    -- flight profile going from marshal to fence, and from egrss to RTB
    self.transitSpeed = init["transitSpeed"] or self.primaryGroupMaxSpeed * 0.8
    self.transitAltitude = init["transitAltitude"] or UTIL.FeetToMeters(20000)

    -- flight profile from fence to ingress to attack to egress
    self.tacticalSpeed = init["tacticalSpeed"] or self.primaryGroupMaxSpeed
    self.tacticalAltitude = init["tacticalAltitude"] or UTIL.FeetToMeters(10000)

    return self
end
-- }}}2

function cobralake.StrikeMissionGenerator:onSpawnMainGroup(group) -- {{{2
    self:buildStrikeRoute(group)
end
-- }}}2

function cobralake.StrikeMissionGenerator:buildStrikeRoute(strikeGroup) -- {{{2

    local targetGroup = nil
    if self.targetGroup ~= nil then
        targetGroup = self.targetGroup
    elseif self.targetGroupFn ~= nil then
        targetGroup = self.targetGroupFn()
    end

    local strikeTasks = {}
    local engageZones = nil
    if targetGroup ~= nil then
        strikeTasks[#strikeTasks+1] = strikeGroup:TaskAttackGroup(targetGroup)
        if self.engageZones ~= nil then
            engageZones = self.engageZones
        else
            engageZones = {ZONE_GROUP:New(string.format("%s moving zone for strike engagement", targetGroup.GroupName), targetGroup, self.movingEngageZoneRadius)}
        end
    else
        _cobralake_warn("Strike group has no target!")
    end
    if engageZones == nil then
        _cobralake_warn("Nil strike engage zone!")
    end

    local waypoints = {}
    for i, wpProfile in ipairs{
            {self.marshalZones,    self.transitAltitude,  self.transitSpeed,  {}},
            {self.fenceZones,      self.transitAltitude,  self.transitSpeed,  {}},
            {self.ingressZones,    self.tacticalAltitude, self.tacticalSpeed, {}},
            {engageZones,          self.tacticalAltitude, self.tacticalSpeed, strikeTasks},
            {self.egressZones,     self.tacticalAltitude, self.tacticalSpeed, {}},
            {self.rtbZones,        self.transitAltitude,  self.transitSpeed,  {}},
        } do
        cobralake.utils.addRandomAirWaypointFromZones{
            waypoints=waypoints,
            zones=wpProfile[1],
            altitude=wpProfile[2],
            -- altitudeType="RADIO",
            speed=wpProfile[3],
            tasks=wpProfile[4]}
    end
    if self.landingAirbase ~= nil then
        -- approach #1
        local landingWp = self.landingAirbase:GetCoordinate():SetAltitude(100):WaypointAirLanding(400, self.landingAirbase, {}, "Landing")
        waypoints[#waypoints + 1] = landingWp
    end
    --  In case we need to land and destroy the group(s)
        -- https://github.com/FlightControl-Master/MOOSE_MISSIONS_UNPACKED/blob/master/SET%20-%20Data%20Sets/SET-UNT/SET-UNT-120%20-%20FilterOnce%20Scenario%203/SET-UNT-120%20-%20FilterOnce%20Scenario%203.lua
        -- GroupPlanes = GROUP:FindByName( "Planes 1" )
        -- GroupPlanes:HandleEvent( EVENTS.EngineShutdown )
        -- function GroupPlanes:OnEventEngineShutdown( EventData )
        --   EventData.IniUnit:Destroy()
        -- end
    strikeGroup:Route(waypoints, 1)
end
-- }}}2

-- }}}1

-- cobralake.BaseMissionScheduler  {{{1
----------------------------------------------------------------------------------

cobralake.BaseMissionScheduler = {}
cobralake.BaseMissionScheduler.__index = cobralake.BaseMissionScheduler

setmetatable(cobralake.BaseMissionScheduler, {
    __call = function (cls, ...)
        return cls.new(...)
    end,
})

function cobralake.BaseMissionScheduler.new(init)
    local self = setmetatable({}, cobralake.BaseMissionScheduler)
    self.missionGenerators = {}
    self.name = init["name"] or "Scheduler"
    self.waitingTimeFn = init["waitingTimeFn"] or nil
    self.initialTimeRange = init["initialTimeRange"] or {-1, -1}
    self.waitingTimeRange = init["waitingTimeRange"]
    self.schedulerMissionLimit = init["schedulerMissionLimit"] or nil
    self.schedulerMaxConcurrentMissionFn = init["schedulerMaxConcurrentMissionFn"] or nil
    self.schedulerMaxConcurrentMissions = init["schedulerMaxConcurrentMissions"] or nil
    self.alliedCoalitionId = init["alliedCoalitionId"] -- the coalition launching the mission, e.g. coalition.side.RED, (for messaging)
    self.enemyCoalitionId = init["enemyCoalitionId"] -- the coalition the mission is launched against, e.g. coalition.side.BLUE (for messaging)
    self.enemyAlertMessageDelayRange = init["enemyAlertMessageDelayRange"] or {10, 300}
    self.numMissionsGenerated = 0
    self.numScheduledLaunchCycles = 0
    self._isLaunchesPaused = false
    self.pulseSize = init["pulseSize"] or 1
    self.pulseSizeFn = init["pulseSizeFn"] or nil
    return self
end

function cobralake.BaseMissionScheduler:newStrikeMissionGenerator(init)
    return self:addMissionGenerator(cobralake.StrikeMissionGenerator(init))
end

function cobralake.BaseMissionScheduler:newInterdictionMissionGenerator(init)
    return self:addMissionGenerator(cobralake.InterdictionMissionGenerator(init))
end

function cobralake.BaseMissionScheduler:addMissionGenerator(sg)
    self.missionGenerators[#self.missionGenerators+1] = sg
    return sg
end

function cobralake.BaseMissionScheduler:getRandomMissionWaitingTime()
    local t = 1
    if self.waitingTimeFn ~= nil then
        t = self.waitingTimeFn(self)
    else
        t = math.random(self.waitingTimeRange[1], self.waitingTimeRange[2])
    end
    return t
end

function cobralake.BaseMissionScheduler:getInitialWaitingTime()
    local firstMissionDelay = 1
    if self.waitingTimeFn ~= nil then
        firstMissionDelay = self.waitingTimeFn(self)
    elseif self.initialTimeRange ~= nil and self.initialTimeRange[1] >= 0 then
        firstMissionDelay = math.random(self.initialTimeRange[1], self.initialTimeRange[2])
    else
        firstMissionDelay = self:getRandomMissionWaitingTime()
    end
    return firstMissionDelay
end

function cobralake.BaseMissionScheduler:start()
    timer.scheduleFunction(self.launchMission, self, timer.getTime() + self:getInitialWaitingTime())
end

function cobralake.BaseMissionScheduler:pauseLaunches()
    self._isLaunchesPaused = true
end

function cobralake.BaseMissionScheduler:resumeLaunches()
    self._isLaunchesPaused = true
end

function cobralake.BaseMissionScheduler:launchMission(isNoAlert)
    self.numScheduledLaunchCycles = self.numScheduledLaunchCycles+ 1
    local pulseSize = self.pulseSize
    if self.pulseSizeFn ~= nil then
        pulseSize = self.pulseSizeFn(self)
    end
    for midx = 1, pulseSize do
        if self.schedulerMissionLimit ~= nil and self.numMissionsGenerated >= self.schedulerMissionLimit then
            return nil
        end
        local totalActiveMissions = 0
        if not self._isLaunchesPaused then
            local operational_missionGenerators = {}
            for i = 1, #self.missionGenerators do
                local mg = self.missionGenerators[i]
                local nam = mg:numActiveMissions()
                totalActiveMissions = totalActiveMissions + nam
                if mg:isOperational() then
                    operational_missionGenerators[#operational_missionGenerators+1] = mg
                end
            end
            if not self:isMaxActiveMissionLimit(totalActiveMissions) then
                if #operational_missionGenerators > 0 then
                    local mg = operational_missionGenerators[ math.random(1, #operational_missionGenerators) ]
                    self:generateMission(mg, isNoAlert)
                end
            end
        end
    end
    return timer.getTime() + self:getRandomMissionWaitingTime()
end

function cobralake.BaseMissionScheduler:isMaxActiveMissionLimit(totalActiveMissions)
    local schedulerMaxConcurrentMissions = nil
    if self.schedulerMaxConcurrentMissionFn ~= nil then
        schedulerMaxConcurrentMissions = self.schedulerMaxConcurrentMissionFn(self)
    elseif self.schedulerMaxConcurrentMissions ~= nil then
        schedulerMaxConcurrentMissions = self.schedulerMaxConcurrentMissions
    end
    if schedulerMaxConcurrentMissions == nil or totalActiveMissions < schedulerMaxConcurrentMissions then
        return false
    else
        return true
    end
end

function cobralake.BaseMissionScheduler:generateMission(sg, isNoAlert)
    local result = sg:generate()
    if result ~= nil and result.status then
        if not isNoAlert then
            self:sendAlert(result.primaryGroup, result.escortGroup)
        end
        self.numMissionsGenerated = self.numMissionsGenerated + 1
    end
    self:onMissionGeneration(result)
    return result
end

function cobralake.BaseMissionScheduler:sendAlert(primaryGroup, escortGroup) -- {{{2
    if self.enemyAlertMessageDelayRange ~= nil and self.enemyCoalitionId ~= nil then
        local messageDelay = math.random(self.enemyAlertMessageDelayRange[1], self.enemyAlertMessageDelayRange[2])
        local missionUnits = primaryGroup:GetUnits()
        local missionUnit = missionUnits[1]
        local numUnits = #missionUnits
        if escortGroup ~= nil then
            local escortUnits = escortGroup:GetUnits()
            numUnits = numUnits + #escortUnits
        end
        local unitTypeDesc = missionUnit:GetTypeName()
        local message = cobralake.utils.composeMissionEnemyAlertMessage(
                numUnits,
                unitTypeDesc,
                missionUnit:GetCoordinate(),
                primaryGroup.GroupName,
                "reported")
        cobralake.utils.sendDelayedMessageToCoalition(messageDelay, self.enemyCoalitionId, message)
    end
end
-- }}}2

function cobralake.BaseMissionScheduler:launchAll(pulseSize, isNoAlert)
    if pulseSize == nil then
        pulseSize = 1
    end
    for midx = 1, pulseSize do
        for i = 1, #self.missionGenerators do
            local check_sg = self.missionGenerators[i]
            if check_sg:isOperational() then
                local result = self:generateMission(check_sg, isNoAlert)
            end
        end
    end
end

function cobralake.BaseMissionScheduler:onMissionGeneration(missionGenerationResult)
end

-- }}}1

-- cobralake.TimeDrivenMissionScheduler {{{1
----------------------------------------------------------------------------------

cobralake.TimeDrivenMissionScheduler = {} -- {{{2
cobralake.TimeDrivenMissionScheduler.__index = cobralake.TimeDrivenMissionScheduler
setmetatable(cobralake.TimeDrivenMissionScheduler, {
    __index=cobralake.BaseMissionScheduler,
    __call = function (cls, ...)
    return cls.new(...)
    end,
})
-- }}}2

function cobralake.TimeDrivenMissionScheduler.new(init) -- {{{2
    local baseclass = cobralake.BaseMissionScheduler(init)
    local self = setmetatable(baseclass, cobralake.TimeDrivenMissionScheduler)
    return self
end
-- }}}2

--  }}}1

-- cobralake.CountDrivenMissionScheduler {{{1
----------------------------------------------------------------------------------
--  Instead of launching missions on a schedule (whether randomized or not), this
--  class manages missions by count, ensuring the specified number of mission are
--  active at any point. Same variables as the TimeDrivenMissionScheduler, but
--  priorities are reversed. E.g., the TimeDrivenMissionScheduler will launch
--  missions on schedule except if the active mission limit is reached. However,
--  if the active mission limit is not reached, it will not launch any missions
--  until the waiting time is expired. This one will launch a mission if the
--  number of active missions drops below the limit, with "waitingTime" here used
--  as a delay. This results in a better player experience. With the TimeDriven
--  mission scheduler, a high waiting time means that there might be a lot of
--  dead time for the player (after shooting down an enemy or the enemy RTB's),
--  while a low waiting time means the player is overwhelmed (at the equilibrium
--  state of max active mission count, as soon as they shoot down an enemy,
--  another one pops up). The issue is that enemy loss is independent of the
--  replacement schedule. It might be possible to come up with a perfect waiting
--  time, but that takes a lot of tuning and will probably be specific to
--  different circumstances. A count-driven mission scheduler makes it simpler by
--  typing the replacement schedule (triggering it, actually) to enemy loss.. The
--  pressure can be kept up on the player by keeping up the number of enemy
--  missions and the waiting time is just the delay for an enemy replacement.

cobralake.CountDrivenMissionScheduler = {} -- {{{2
cobralake.CountDrivenMissionScheduler.__index = cobralake.CountDrivenMissionScheduler
setmetatable(cobralake.CountDrivenMissionScheduler, {
    __index=cobralake.BaseMissionScheduler,
    __call = function (cls, ...)
    return cls.new(...)
    end,
})
-- }}}2

function cobralake.CountDrivenMissionScheduler.new(init) -- {{{2
    local baseclass = cobralake.BaseMissionScheduler(init)
    local self = setmetatable(baseclass, cobralake.CountDrivenMissionScheduler)
    self.checkFrequency = init["checkFrequency"] or 600
    self.activeMissionGroups = {}
    return self
end
-- }}}2

function cobralake.CountDrivenMissionScheduler:start() -- {{{2
    timer.scheduleFunction(self.check, self, timer.getTime() + self:getInitialWaitingTime())
end
-- }}}2

function cobralake.CountDrivenMissionScheduler:pollActiveMissions() -- {{{2
    local newActiveMissionGroups = {}
    for i = 1, #self.activeMissionGroups do
        local group = self.activeMissionGroups[i]
        if group and group ~= nil and group:IsAlive() then
            newActiveMissionGroups[#newActiveMissionGroups+1] = group
        end
    end
    return newActiveMissionGroups
end
-- }}}2

function cobralake.CountDrivenMissionScheduler:check() -- {{{2
    if self.schedulerMissionLimit ~= nil and self.numMissionsGenerated >= self.schedulerMissionLimit then
        return nil
    end
    self.activeMissionGroups = self:pollActiveMissions()
    if not self:isMaxActiveMissionLimit(#self.activeMissionGroups) then
        timer.scheduleFunction(
            function()
                self:launchMission()
                return nil -- one off
            end,
            {},
            timer.getTime() + self:getRandomMissionWaitingTime())
    end
    return timer.getTime() + self.checkFrequency
end
-- }}}2

function cobralake.CountDrivenMissionScheduler:onMissionGeneration(missionGenerationResult) -- {{{2
    local primaryGroup = missionGenerationResult.primaryGroup
    self.activeMissionGroups[#self.activeMissionGroups+1] = primaryGroup
end
-- }}}2

-- }}}1

-- cobralake.MissionCallsignGenerator  {{{1
----------------------------------------------------------------------------------

cobralake.MissionCallsignGenerator = {} -- {{{2
cobralake.MissionCallsignGenerator.__index = cobralake.MissionCallsignGenerator
-- }}}2

setmetatable(cobralake.MissionCallsignGenerator, { -- {{{2
    __call = function (cls, ...)
        return cls.new(...)
    end,
})
-- }}}2

function cobralake.MissionCallsignGenerator.new(init) -- {{{2
    local self = setmetatable({}, cobralake.MissionCallsignGenerator)
    self.callsignNameIndex = init["callsignNameIndex"] or 1
    self.callsignNameString = cobralake.data.callsignNames.aircraft[self.callsignNameIndex] or "Diablo"
    self.queueGenerationNextStartIndex = 1
    self.queueGenerationBatchSize = 9
    self.callsignQueue = cobralake.Queue()
    self:replenishQueue()
    return self
end
-- }}}2

function cobralake.MissionCallsignGenerator:replenishQueue() -- {{{2
    for idx=self.queueGenerationNextStartIndex, self.queueGenerationNextStartIndex+self.queueGenerationBatchSize-1 do
        self.callsignQueue:push({
            callsignNameIndex=self.callsignNameIndex,
            callsignNumber=idx,
            callsignNameString=self.callsignNameString,
            groupCallsignString=string.format("%s %s", self.callsignNameString, idx),
        })
    end
    self.queueGenerationNextStartIndex = self.queueGenerationNextStartIndex + self.queueGenerationBatchSize
    return self.callsignQueue
end
-- }}}2

function cobralake.MissionCallsignGenerator:acquireCallsignComponents() -- {{{2
    if self.callsignQueue:isEmpty() then
        self:replenishQueue()
    end
    local newcallsign = self.callsignQueue:pop()
    return newcallsign
end
-- }}}2

function cobralake.MissionCallsignGenerator:recycleCallsignComponents(callsign) -- {{{2
    self.callsignQueue:push(callsign)
end
-- }}}2


-- }}}1

-- cobralake.CombatAirPatrolSector  {{{1
----------------------------------------------------------------------------------

cobralake.CombatAirPatrolSector = {} -- {{{2
cobralake.CombatAirPatrolSector.__index = cobralake.CombatAirPatrolSector
-- }}}2

setmetatable(cobralake.CombatAirPatrolSector, { -- {{{2
    __call = function (cls, ...)
        return cls.new(...)
    end,
})
-- }}}2

function cobralake.CombatAirPatrolSector.new(init) -- {{{2
    local self = setmetatable({}, cobralake.CombatAirPatrolSector)
    self.capStationName = init["capStationName"] or "CAP Station"
    self.callsignNameIndex = init["callsignNameIndex"] or 1
    self.radioFrequency = init["radioFrequency"]
    self.callsignGenerator = cobralake.MissionCallsignGenerator{callsignNameIndex=self.callsignNameIndex}
    self.isAutostart = init["isAutostart"]
    if self.isAutostart == nil then
        self.isAutostart = true
    end
    self.isRepeatByDefault = init["isRepeatByDefault"]
    if self.isRepeatByDefault == nil then
        self.isRepeatByDefault = true
    end

    -- Spawn order of priority:
    -- (1) If spawn zone is provided: use this
    self.capSpawnZone = init["capSpawnZone"] or nil
    if self.capSpawnZone == nil then
        self.capSpawnZoneName = init["capSpawnZoneName"] or nil
        if self.capSpawnZoneName ~= nil then
            self.capSpawnZone = ZONE:New(self.capSpawnZoneName)
        end
    end

    self.patrolZone = init["patrolZone"]
    if self.patrolZone == nil then
        self.patrolZoneName = init["patrolZoneName"]
        self.patrolZone = ZONE:New(self.patrolZoneName)
    else
        self.patrolZoneName = self.patrolZone:GetName()
    end
    if self.engageZone == nil and init["engageZoneName"] ~= nil then
        self.engageZoneName = init["engageZoneName"]
        self.engageZone = ZONE:New(self.engageZoneName)
    else
        self.engageZoneName = self.engageZone:GetName()
    end

    self.patrolFloorAltitude = init["patrolFloorAltitude"] or 7620 -- 25K feet
    self.patrolCeilingAltitude = init["patrolCeilingAltitude"] or 9144 -- 30K feet
    self.patrolMinSpeed = init["patrolMinSpeed"] or 926 -- 500 knots
    self.patrolMaxSpeed = init["patrolMaxSpeed"] or 1111 -- 600 knots
    self.patrolAltType = init["patrolAltType"] or "BARO"

    self.numDeployedGroups = 0

    return self
end
-- }}}2

function cobralake.CombatAirPatrolSector:acquireCallsignComponents() -- {{{2
    local c = self.callsignGenerator:acquireCallsignComponents()
    return c
end
-- }}}2

function cobralake.CombatAirPatrolSector:recycleCallsignComponents(callsign) -- {{{2
    self.callsignGenerator:recycleCallsignComponents(callsign)
end
-- }}}2

-- }}}1

-- cobralake.CapControl  {{{1
----------------------------------------------------------------------------------

cobralake.CapControl = {} -- {{{2
cobralake.CapControl.__index = cobralake.CapControl
-- }}}2

setmetatable(cobralake.CapControl, { -- {{{2
    __call = function (cls, ...)
        return cls.new(...)
    end,
})
-- }}}2

function cobralake.CapControl.new(init) -- {{{2
    local self = setmetatable({}, cobralake.CapControl)
    self.assetGroupName = init["assetGroupName"]
    self.assetGroupAlias = init["assetGroupAlias"] or cobralake.utils.ensureUniqueAlias(self.assetGroupName)
    self.assetGroupSize = GROUP:FindByName(self.assetGroupName):GetInitialSize()
    self.assetGroupInventoryTracker = init["assetGroupInventoryTracker"]
    self.coalitionId = init["coalitionId"]
    _assertNotNil(self, "coalitionId")
    -- self.callsignNameIndex = init["callsignNameIndex"]
    self.callsignNameString = "COBRALAKE"
    self.initialDelay = init["initialDelay"] or 1
    self.missionQueueCheckFrequency = 120 -- high rate for debugging
    self.patrolEventCheckFrequency = 60 -- high rate for debugging

    -- Spawn order of priority:
    -- If spawn zone is given to specific CapAirPatrolSector, then use that.
    -- (1) Otherwise, if isCapStartTakeoff is not nil but one of SPAWN.TakeoffRunway, SPAWN.TakeoffHot, etc.,
    -- then base/carrier takeoff
    self.isCapStartTakeoff = init["isCapStartTakeoff"]
    -- (2) Otherwise, isCapSpawnInPatrolZone is true then spawn directly in patrol zone
    self.isCapSpawnInPatrolZone = init["isCapSpawnInPatrolZone"]
    -- (3) Otherwise, if isCapSpawnAfterClearingTurn is true, then spawn near carrier
    self.isCapSpawnAfterClearingTurn = init["isCapSpawnAfterClearingTurn"]
    -- (4) Otherwise, spawn in ME position

    -- when out of fuel/ammo, do we recover ("recover") or just despawn ("despawn") ?
    self.isRecoverOrDespawnOnRtb = init["isRecoverOrDespawnOnRtb"] or "despawn"

    -- homeAirbase
    self.homeAirbase = init["homeAirbase"]
    if self.homeAirbase == nil then
        self.homeAirbaseName = init["homeAirbaseName"]
        self.homeAirbase = AIRBASE:FindByName(self.homeAirbaseName)
    end
    self.assetGroupSpawner = SPAWN:NewWithAlias(self.assetGroupName, self.assetGroupAlias)
    self.assetGroupSpawner:InitCleanUp(300)
    self.capStations = {}
    self.missionQueue = cobralake.Queue()
    return self
end
-- }}}2

function cobralake.CapControl:startMissionQueue() -- {{{2
    self.missionQueueFnId = timer.scheduleFunction(
        function()
            if self.assetGroupInventoryTracker.allocation == 0 then
                return nil
            end
            if self.missionQueue:isEmpty() then
                return timer.getTime() + self.missionQueueCheckFrequency
            end
            if self.assetGroupInventoryTracker ~= nil and not self.assetGroupInventoryTracker:hasAvailable(self.assetGroupSize) then
                return timer.getTime() + self.missionQueueCheckFrequency
            end
            local missionRequest = self.missionQueue:pop()
            local capGroup = self:spawnPatrolGroup(missionRequest)
            if capGroup == nil then
                self.missionQueue:push(missionRequest)
                return timer.getTime() + self.missionQueueCheckFrequency
            end
            self:assignPatrol(capGroup)
            return timer.getTime() + self.missionQueueCheckFrequency
        end, {}, timer.getTime() + self.initialDelay)
end --}}}2

function cobralake.CapControl:newCapStation(init) -- {{{2
    local params = cobralake.utils.table.shallowCopy(init)
    -- params.capStationName = init.capStationName
    -- params.callsignNameIndex = init.callsignNameIndex or self.callsignNameIndex
    -- params.capSpawnZone = init.capSpawnZone
    -- params.capSpawnZoneName = init.capSpawnZoneName
    -- params.patrolZone = init.patrolZone
    -- params.patrolZoneName = init.patrolZoneName
    -- params.engageZone = init.engageZone
    -- params.engageZoneName = init.engageZoneName
    params.patrolFloorAltitude = init["patrolFloorAltitude"] or self.patrolFloorAltitude
    params.patrolCeilingAltitude = init["patrolCeilingAltitude"] or self.patrolCeilingAltitude
    params.patrolMinSpeed = init["patrolMinSpeed"] or self.patrolMinSpeed
    params.patrolMaxSpeed = init["patrolMaxSpeed"] or self.patrolMaxSpeed
    params.patrolAltType = init["patrolAltType"] or self.patrolAltType
    local capStation = cobralake.CombatAirPatrolSector(params)
    local capStationName = capStation.capStationName
    self.capStations[capStationName] = capStation
end
-- }}}2

function cobralake.CapControl:startCombatAirPatrols() -- {{{2
    self:rebuildMissionQueue()
    self:startMissionQueue()
end
-- }}}2

function cobralake.CapControl:rebuildMissionQueue() -- {{{2
    self.missionQueue:clear()
    for _, capStation in pairs(self.capStations) do
        if capStation.isAutostart then
            self:addMissionToQueue(capStation.capStationName, capStation.isRepeatByDefault)
        end
    end
end
-- }}}2

function cobralake.CapControl:removeMissionFromQueue(capStationName) -- {{{2
    local newQueue = cobralake.Queue()
    local numRemoved = 0
    local numKept = 0
    while not self.missionQueue:isEmpty() do
        local missionRequest = self.missionQueue:pop()
        if missionRequest.capStation.capStationName == capStationName then
            numRemoved = numRemoved + 1
        else
            numKept = numKept + 1
            newQueue.push(missionRequest)
        end
    end
    self.missionQueue = newQueue
    return {
        numRemovedFromQueue = numRemoved,
        numKeptOnQueue = numKept
    }
end
-- }}}2

function cobralake.CapControl:stopRepeatingActiveMission(capStationName, isCancelCurrentMission) -- {{{2
    local numRemoved = 0
    local numKept = 0
    local group, index = self.assetGroupSpawner:GetFirstAliveGroup()
    while group ~= nil do
        if group.capStation.capStationName == capStationName then
            numRemoved = numRemoved + 1
            group.isRepeat = false
            if isCancelCurrentMission then
                group._is_scrub_mission = true
            end
        else
            numKept = numKept + 1
        end
        group, index = self.assetGroupSpawner:GetNextAliveGroup(index)
    end
    return {
        numRemovedFromStation = numRemoved,
        numKeptOnStation = numKept
    }
end
-- }}}2

function cobralake.CapControl:scrubCapStationMissions(capStationName, isCancelCurrentMission) -- {{{2
    local r1 = self:removeMissionFromQueue(capStationName)
    local r2 = self:stopRepeatingActiveMission(capStationName, isCancelCurrentMission)
    local r3 = {}
    for k, v in pairs(r1) do
        r3[k] = r1[k]
    end
    for k, v in pairs(r2) do
        r3[k] = r2[k]
    end
    return r3
end
-- }}}2

function cobralake.CapControl:addMissionToQueue(capStationName, isRepeat) -- {{{2
    if capStationName == nil then
        error("capStationName is 'nil'")
        return
    end
    if isRepeat == nil then
        isRepeat = false
    end
    local capStation = self.capStations[capStationName]
    if capStation == nil then
        _cobralake_warn(string.format("CapStation '%s' not found!", capStationName))
    else
        local missionRequest = {
            capStation=capStation,
            isRepeat=isRepeat,
        }
        self.missionQueue:push(missionRequest)
    end
end
-- }}}2

function cobralake.CapControl:clearMissionQueue() -- {{{2
    self.missionQueue:clear()
end
-- }}}2

function cobralake.CapControl:spawnPatrolGroup(params) -- {{{2
    local capStation = params["capStation"]
    local isRepeat = params["isRepeat"]
    local capStationName = capStation.capStationName
    local capSpawnZone = capStation.spawnZone or nil
    local patrolIdentifier = string.format("%s (%s/%s)", capStationName, capStation.numDeployedGroups+1, capStation.numDeployedGroups)
    if self.assetGroupInventoryTracker ~= nil and not self.assetGroupInventoryTracker:hasAvailable(self.assetGroupSize) then
        return nil
    end

    local capGroup = nil
    local isNeedsRouteToPatrolZone = true
    local isInPatrolZone = false
    if capSpawnZone ~= nil then
        capGroup = self.assetGroupSpawner:SpawnInZone(capSpawnZone, true, self.patrolFloorAltitude, self.patrolCeilingAltitude)
    elseif false then -- self.isCapStartTakeoff ~= nil then
        capGroup = self.assetGroupSpawner:SpawnAtAirbase(self.homeAirbase, self.capStartTakeoff)
    elseif self.isCapSpawnInPatrolZone then
        capGroup = self.assetGroupSpawner:SpawnInZone(capStation.patrolZone, true, self.patrolFloorAltitude, self.patrolCeilingAltitude)
        isInPatrolZone = true
        isNeedsRouteToPatrolZone = false
    elseif false then -- self.isCapSpawnAfterClearingTurn then
        local originUnit = self.homeAirbase
        if self.homeAirbaseUnit ~= nil then
            originUnit = self.homeAirbaseUnit
        end
        local originUnitHeading = 0
        if originUnit.GetHeading ~= nil then
            originUnitHeading = originUnit:GetHeading()
        end
        local originUnitCoordinate = originUnit:GetCoordinate()
        -- nautical miles (13 km; 8.1 mi)
        local spawnCoordinate = originUnitCoordinate:Translate(originUnitHeading + 15, 15000)
        -- spawnCoordinate.y = 152  -- 500ft for clearing turnspawnCoordinate.y
        spawnCoordinate.y = self.patrolFloorAltitude  -- should be 501ft at clearing; but a low number here causes weird behavior (e.g., a/c try to land)
        local spawnVec3 = spawnCoordinate:GetVec3()
        capGroup = self.assetGroupSpawner:SpawnFromVec3(spawnVec3)

    else
        capGroup = self.assetGroupSpawner:Spawn()
    end
    if capGroup == nil then
        return nil
    end

    if self.assetGroupInventoryTracker:checkoutGroup(capGroup, true) == nil then
        return nil
    end

    capGroup.capControl = self
    capGroup.capStation = capStation
    capGroup.isRepeat = isRepeat
    if isNeedsRouteToPatrolZone then
        capGroup:Route(self:buildRouteToPatrolZone(),1)
    end
    self:setupPatrolGroupEventHandlers(capGroup)
    capGroup._enteredPatrolZone = isInPatrolZone
    return capGroup
end
-- }}}2

function cobralake.CapControl:assignPatrol(capGroup) -- {{{2
    local capStation = capGroup.capStation
    capGroup.capStation.numDeployedGroups = capGroup.capStation.numDeployedGroups + 1
    capGroup.missionCallsignComponents = capStation:acquireCallsignComponents()
    capGroup:CommandSetCallsign(
        capGroup.missionCallsignComponents.callsignNameIndex,
        capGroup.missionCallsignComponents.callsignNumber,
        0)
    if capStation.radioFrequency ~= nil then
        capGroup:CommandSetFrequency(capStation.radioFrequency[1], capStation.radioFrequency[2])
    end
    if capGroup._enteredPatrolZone then -- b/c spawned in patrol zone
        self:sendMessageFromGroup(capGroup,
            string.format("on %s station, at %s", capStation.capStationName, cobralake.utils.composeSpatialReference( capGroup:GetCoordinate(), 4, 10)))
    end

    local capZone = AI_CAP_ZONE:New(
        capStation.patrolZone, -- @param Core.Zone#ZONE_BASE PatrolZone The @{Zone} where the patrol needs to be executed.
        capStation.patrolFloorAltitude, -- @param DCS#Altitude PatrolFloorAltitude The lowest altitude in meters where to execute the patrol.
        capStation.patrolCeilingAltitude, -- @param DCS#Altitude PatrolCeilingAltitude The highest altitude in meters where to execute the patrol.
        capStation.patrolMinSpeed, -- @param DCS#Speed  PatrolMinSpeed The minimum speed of the @{Wrapper.Controllable} in km/h.
        capStation.patrolMaxSpeed -- @param DCS#Speed  PatrolMaxSpeed The maximum speed of the @{Wrapper.Controllable} in km/h.
        -- "BARO" -- @param DCS#AltitudeType PatrolAltType The altitude type ("RADIO"=="AGL", "BARO"=="ASL"). Defaults to RADIO
    )
    capZone:SetControllable(capGroup)
    capGroup.capZone = capZone
    if capStation.engageZone ~= nil then
        capZone:SetEngageZone(capStation.engageZone)
    else
        if capStation.engageRange ~= nil then
            capStation.engageRange = 200000
        else
            capZone:SetEngageRange(capStation.engageRange)
        end
    end

    function capZone:OnAfterRTB( AIGroup )
        if not AIGroup.isDeployed then
            return
        end
        AIGroup.capControl:sendMessageFromGroup(AIGroup, "RTB")
        AIGroup.capControl:rtbPatrol(AIGroup)
        -- NOTE!!! ``self`` here will dereference to the ``capZone`` object,
        -- NOT the CapControl instance!
        if AIGroup.isRepeat then
            AIGroup.capControl:replacePatrol(AIGroup)
        end
    end

    capZone:__Start(1)

    --- schedule a function to check to see if CAP needs to be cycled/replaced
    local fid = timer.scheduleFunction(
        function()
            if capGroup == nil then
                return nil
            end
            if not capGroup.isDeployed then
                return nil
            end
            local ammo = cobralake.utils.ammoAvailability(capGroup)
            local missilesRemaining = 0
            if ammo ~= nil then
                missilesRemaining = ammo.missiles
            end
            local isDefunct = false
            if not capGroup._enteredPatrolZone then
                -- :IsPartlyOrCompletelyInZone()
                if capGroup:IsCompletelyInZone(capStation.patrolZone) and not capGroup.isNotAnnounceInitiation then
                    self:sendMessageFromGroup(capGroup, string.format("on %s station, at %s",
                        capStation.capStationName,
                        cobralake.utils.composeSpatialReference( capGroup:GetCoordinate(), 4, 10)
                        ))
                    capGroup._enteredPatrolZone = true
                end
            end
            if not capGroup:IsAlive() then
                self:sendMessage(string.format("%s is down!", capGroup.missionCallsignComponents.groupCallsignString))
                isDefunct = true
            elseif missilesRemaining == 0 or capGroup._is_scrub_mission then
                -- TODO: find first alive unit and report here
                if capGroup._is_scrub_mission then
                    self:sendMessageFromGroup(capGroup, "scrubbing mission and RTB")
                    capGroup._is_scrub_mission = nil
                else
                    self:sendMessageFromGroup(capGroup, "winchester and RTB")
                end
                self:rtbPatrol(capGroup)
                isDefunct = true
            end
            if isDefunct then
                capZone:__Accomplish(1)
                self:unassignPatrol(capGroup)
                if capGroup.isRepeat then
                    self:replacePatrol(capGroup)
                end
                return nil -- return value of nil: do not run this function again (as this CAP is done)
            else
                -- cap replacement not needed
                return timer.getTime() + self.patrolEventCheckFrequency  --- return value => run check again in 600 seconds
            end
        end,
        {}, timer.getTime() + self.patrolEventCheckFrequency) --- run first check in 600 seconds
    capGroup.isDeployed = true
    return capGroup
end
-- }}}2

function cobralake.CapControl:rtbPatrol(capGroup) -- {{{2
    -- https://flightcontrol-master.github.io/MOOSE_DOCS_DEVELOP/Documentation/Wrapper.Controllable.html##(CONTROLLABLE).TaskFunction
    -- https://flightcontrol-master.github.io/MOOSE_DOCS_DEVELOP/Documentation/Wrapper.Controllable.html##(CONTROLLABLE).SetTaskWaypoint
    self:unassignPatrol(capGroup)
    if self.isRecoverOrDespawnOnRtb == "recover" then
        local landingWp = self.homeAirbase:GetCoordinate():SetAltitude(400):WaypointAirLanding(400, self.homeAirbase, {}, "Landing")
        capGroup:Route({landingWp}, 1)
    else
        self.assetGroupInventoryTracker:checkinAndDespawnGroup(capGroup, false)
    end
end
-- }}}2

function cobralake.CapControl:replacePatrol(capGroup) -- {{{2
    local missionRequest = {
        capStation=capGroup.capStation,
        isRepeat=capGroup.isRepeat,
    }
    self.missionQueue:push(missionRequest)
end
-- }}}2

function cobralake.CapControl:unassignPatrol(capGroup) -- {{{2
    if not capGroup.isDeployed then
        return nil
    end
    capGroup.capStation:recycleCallsignComponents(capGroup.missionCallsignComponents)
    capGroup.capStation.numDeployedGroups = capGroup.capStation.numDeployedGroups - 1
    if capGroup.capStation.numDeployedGroups < 0 then
        capGroup.capStation.numDeployedGroups = 0
    end
    capGroup.isDeployed = false
end
-- }}}2

function cobralake.CapControl:setupPatrolGroupEventHandlers(group) -- {{{2
    -- AnyShot = EVENTHANDLER:New() --Declare "AnyShot" as a New Event Handler
    -- AnyShot:HandleEvent( EVENTS.Shot ) --Subscribe to the event type of "OnShot"
    -- function AnyShot:OnEventShot( EventData ) --Create the function to run when the handler fires on a shot and put the event into the table "EventData"
    --      local ThisUnit = UNIT:FindByName(EventData.IniDCSUnitName) --Find the "IniDCSUnitName" in the EventData table
    --      local ThisGroup = GROUP:FindByName(EventData.IniDCSGroupName) --Find the "IniDCSGroupName" in the EventData table
    --      if EventData.WeaponName == "weapons.bombs.AIRBORNE_3_PARASHUT_BOX" then --Do some logic on the returned event data's WeaponName
    --          --do more stuff
    --      end
    -- end
    local units = group:GetUnits()
    for i=1, #units do
        local unit = units[i]
        unit:HandleEvent(EVENTS.Shot)
        function unit:OnEventShot( EventData )
            -- EventData.IniUnit is the unit making the shot
            -- EventData.TgtUnit is the unit that was hit
            -- Target = EventData.TgtUnit
            -- TgtName = EventData.TgtUnitName
            group.capControl:sendMessageFromUnit(unit, string.format("%s!", cobralake.utils.getShotCall(EventData.WeaponName)))
        end
        unit:HandleEvent(EVENTS.Hit)
        function unit:OnEventHit( EventData )
            local t = timer.getTime()
            if unit._last_hit == nil or t - unit._last_hit > 120 then
                unit._last_hit = t
                group.capControl:sendMessageFromUnit(unit, "I'm hit!")
            end
        end
    end
end
-- }}}2

function cobralake.CapControl:sendMessageFromGroup(group, message, duration) -- {{{2
    local s = ""
    if duration == nil then
        duration = 15
    end
    if group == nil then
        _cobralake_warn("null group!")
        s = string.format("%s: %s", self.callsignNameString, message)
        cobralake.utils.sendMessageToCoalition(self.coalitionId, s)
    else
        local units = group:GetUnits()
        local unit = nil
        -- weirdly, units indexed in opposite order to their numbering
        for i=#units,1,-1 do
            unit = units[i]
            if unit:IsAlive() then
                break
            end
        end
        -- local unit = group:GetFirstUnitAlive()
        if unit == nil then
            s = string.format("%s: %s", group.missionCallsignComponents.groupCallsignString, message)
            cobralake.utils.sendMessageToCoalition(self.coalitionId, s)
        else
            self:sendMessageFromUnit(unit, message, duration)
        end
    end
end
-- }}}2

function cobralake.CapControl:sendMessageFromUnit(unit, message, duration) -- {{{2
    local s = ""
    if unit == nil then
        _cobralake_warn("null unit!")
        s = string.format("%s: %s", self.callsignNameString, message)
    else
        local group = unit:GetGroup()
        if group == nil then
            s = string.format("%s: %s", unit:GetCallsign(), message)
        else
            s = string.format("%s %s-%s: %s",
                group.missionCallsignComponents.callsignNameString,
                group.missionCallsignComponents.callsignNumber,
                unit:GetNumber(),
                message)
        end
    end
    if duration == nil then
        duration = 15
    end
    cobralake.utils.sendMessageToCoalition(self.coalitionId, s)
end
-- }}}2

function cobralake.CapControl:sendMessage(message, duration) -- {{{2
    if duration == nil then
        duration = 15
    end
    local s = string.format("%s: %s", self.callsignNameString, message)
    cobralake.utils.sendMessageToCoalition(self.coalitionId, s)
end
-- }}}2

-- }}}1

-- ScLivelyDeck  {{{1
----------------------------------------------------------------------------------
--
-- Original By FunkyFranky, and posted on Discord
--
-- Example Script to spawn statics on the Supercarrier deck depending on recovery ops.
--
-- Positions of of the statics are taken from RedKite. See
--
-- RedKite YT video: https://www.youtube.com/watch?v=XONOFbhwbsU
-- RedKite SC Lua Templates: https://www.digitalcombatsimulator.com/en/files/3309905/
--
-- I recommend to watch the RedKite's YT video to better understand why certain sets of
-- statics are removed when a recovery starts and ends.
--
-- As this is an example to showcase the spawning and removing of statics, the
-- airboss stuff is left at a bare minimum, e.g. no sound files in the mission etc.
--
-- Recovery windows are short (10 min) to view the statics being removed and added.
--
---

ScLivelyDeck = {} -- {{{2
ScLivelyDeck.__index = ScLivelyDeck
setmetatable(ScLivelyDeck, {
    __call = function (cls, ...)
    return cls.new(...)
    end,
})
-- }}}2

function ScLivelyDeck.new(init) -- {{{2
    local self = setmetatable({}, ScLivelyDeck)
    self.carrierUnitName = init["carrierUnitName"]
    self.carrierUnit = UNIT:FindByName(self.carrierUnitName)
    self.aircraft1 = init["aircraft1"] or "F-14B"
    self.aircraft1Livery = init["aircraft1Livery"] -- "VFA-106 high visibility"
    self.aircraft2 = init["aircraft2"] or "FA-18C_hornet" -- some day: "A-6E"?
    self.aircraft2Livery = init["aircraft2livery"] -- "VF-102 Diamondbacks"
    -- Set prefixes so we can delete certain statics easily.
    self.staticPrefix = init["staticPrefix"] or string.format("%s LivelyDeckAutostatic", self.carrierUnitName)
    self.staticPrefixClearDeck    = string.format("%s Clear", self.staticPrefix)
    self.staticPrefixBlockedDeck  = string.format("%s Blocked", self.staticPrefix)
    self.staticPrefixMassLaunch   = string.format("%s MassLaunch", self.staticPrefix)
    self.staticPrefixMassRecovery = string.format("%s MassRecovery", self.staticPrefix)
    self.staticPrefixMassRecoveryCat1 = string.format("%sCat1", self.staticPrefixMassRecovery)
    self.staticPrefixMassRecoveryCat2 = string.format("%sCat2", self.staticPrefixMassRecovery)
    self.spawnIndex = 0
    return self
end
-- }}}2

function ScLivelyDeck:spawnDeck(statics, prefix) -- {{{2
    --- Function that spawns statics on the carrier deck.
    for i,static in pairs(statics) do
        local name=string.format("%s#%d03", prefix, i)  -- This MUST be a UNIQUE name!
        -- Spawn the static object.
        local static=SPAWNSTATIC:NewFromType(static.type, static.category):InitLinkToUnit(self.carrierUnit, static.x, static.y, static.heading):InitLivery(static.livery):InitShape(static.shape):Spawn(nil, name)
    end
end
-- }}}2

function ScLivelyDeck:spawnStatics(params) -- {{{2
    self.spawnIndex = self.spawnIndex + 1
    local staticNamePrefix = params["namePrefix"] or string.format("%s%s", self.staticPrefix, self.spawnIndex)
    local staticType = params["type"]
    local staticCategory = params["category"]
    local staticShape = params["shape"]
    local staticLivery = params["livery"]
    local staticPositions = params["positions"]
    for idx, position in pairs(staticPositions) do
        local name = string.format("%s/%s/%s", staticNamePrefix, self.spawnIndex, idx)  -- This MUST be a UNIQUE name!
        local static = SPAWNSTATIC:NewFromType(staticType, staticCategory)
                        :InitLinkToUnit(self.carrierUnit, position.x, position.y, position.heading)
                        :InitLivery(staticLivery)
                        :InitShape(staticShape)
                        :Spawn(nil, name)
    end
end
-- }}}2

function ScLivelyDeck:wipeStatics(prefix) -- {{{2
--- Function to remove statics.
    if prefix == nil then
        prefix = self.staticPrefix
    end
    local function removestatic(static)
        static:Destroy()
    end
    local set=SET_STATIC:New():FilterPrefixes(prefix):FilterOnce()
    set:ForEachStatic(removestatic)
end
-- }}}2

function ScLivelyDeck:spawnBlockedDeck() -- {{{2
    -- Function that spawns the blocked deck statics.
    self:spawnStatics{
        namePrefix=self.staticPrefixMassClearDeck,
        type="S-3B Tanker",
        category="Planes",
        livery="usaf standard",
        positions={
            {x=-136.44656968401, y=26.462317789963, heading=math.deg(0.034906585039887)},
        },
    }
    self:spawnStatics{
        namePrefix=self.staticPrefixMassClearDeck,
        type=self.aircraft2,
        category="Planes",
        livery=self.aircraft2Livery,
        positions={
            {x=-71.796896190004, y=20.882562899962, heading=math.deg(5.3930673886625)},
            {x=-58.370438072001, y=17.046432009956, heading=math.deg(5.7770398241012)},
            {x=-45.576714306007, y=14.469786549977, heading=math.deg(5.3407075111026)},
        },
    }
    self:spawnStatics{
        namePrefix=self.staticPrefixMassClearDeck,
        type=self.aircraft1,
        category="Planes",
        livery=self.aircraft1Livery,
        positions={
            {x=-123.20911407401, y=26.834156429977, heading=math.deg(0.034906585039887),},
            {x=-104.617182038,   y=33.229781050002, heading=math.deg(4.6949356878647),  },
            {x=-91.664486535999, y=32.607335179986, heading=math.deg(4.6949356878647),  },
        },
    }
end
-- }}}2

function ScLivelyDeck:spawnClearDeck() -- {{{2
    -- Function that spawn the clear deck statics.
    self:spawnStatics{
        namePrefix=self.staticPrefixMassClearDeck,
        type=self.aircraft1,
        category="Planes",
        livery=self.aircraft1Livery,
        positions={
            { x=23.392320767991,  y=31.035356269975,  heading=math.deg(4.7123889803847)},
            { x=33.698838333992,  y=31.61437411001,   heading=math.deg(4.7123889803847)},
            { x=61.561528349994,  y=34.190822379955,  heading=math.deg(3.3335788713092)},
        },
    }
    self:spawnStatics{
        namePrefix=self.staticPrefixMassClearDeck,
        type="E-2C",
        category="Planes",
        livery="E-2D Demo",
        positions={
            {x=8.8025239199924,  y=30.665721859958,  heading=math.deg(4.6949356878647)},
        },
    }
    self:spawnStatics{
        namePrefix=self.staticPrefixMassClearDeck,
        type="SH-60B",
        category="Helicopters",
        livery="standard",
        positions={
            {x=-120.511512843,   y=-25.023610410048, heading=math.deg(1.7976891295542)},
        },
    }
    self:spawnStatics{
        namePrefix=self.staticPrefixMassClearDeck,
        type="AS32-36A",
        category="ADEquipment",
        positions={
            {x=0.755692206003,   y=33.281995239959,  heading=math.deg(4.6600291028249)},
        },
    }
    self:spawnStatics{
        namePrefix=self.staticPrefixMassClearDeck,
        type="AS32-p25",
        category="ADEquipment",
        positions={
            {x=72.724640796994,  y=32.424999079958,  heading=math.deg(5.4279739737024)},
        },
    }
    self:spawnStatics{
        namePrefix=self.staticPrefixMassClearDeck,
        type="AS32-31A",
        category="ADEquipment",
        positions={
            {x=-79.610005513998, y=30.242116749985, heading=math.deg(2.4958208303519)},
            {x=-0.2022494480043, y=18.819578160008, heading=math.deg(4.1713369122664)},
            {x=-111.66933041,    y=29.150888629956, heading=math.deg(5.1138147083434)},
        },
    }
    self:spawnStatics{
        namePrefix=self.staticPrefixMassClearDeck,
        type="Carrier LSO Personell",
        category="Personnel",
        shape="carrier_lso_usa",
        positions={
            {x=-130.61201797701, y=-22.370473980031, heading=math.deg(2.4434609527921)},
        },
    }
    self:spawnStatics{
        namePrefix=self.staticPrefixMassClearDeck,
        type="Carrier LSO Personell 1",
        category="Personnel",
        shape="carrier_lso1_usa",
        positions={
            {x=-129.42353100701, y=-21.789118479996, heading=math.deg(4.2935099599061)},
        },
    }
    self:spawnStatics{
        namePrefix=self.staticPrefixMassClearDeck,
        type="Carrier Seaman",
        category="Personnel",
        shape="carrier_seaman_USA",
        positions={
            {x=-78.473079361007, y=31.255831669958, heading=math.deg(4.7472955654246)},
            {x=-87.794107105001, y=-35.76436746004, heading=math.deg(1.6580627893946)},
        },
    }
    self:spawnStatics{
        namePrefix=self.staticPrefixMassClearDeck,
        type="us carrier tech",
        category="Personnel",
        shape="carrier_tech_USA",
        livery="white",
        positions={
            {x=-129.497732263,   y=-22.656188270019, heading=math.deg(1.850049007114)},
        },
    }
    self:spawnStatics{
        namePrefix=self.staticPrefixMassClearDeck,
        type="us carrier tech",
        category="Personnel",
        shape="carrier_tech_USA",
        livery="white",
        positions={
            {x=-129.497732263,   y=-22.656188270019, heading=math.deg(1.850049007114)},
        },
    }
    self:spawnStatics{
        namePrefix=self.staticPrefixMassClearDeck,
        type="us carrier tech",
        category="Personnel",
        shape="carrier_tech_USA",
        livery="white",
        positions={
            {x=-129.497732263,   y=-22.656188270019, heading=math.deg(1.850049007114)},
            {x=58.869844022993,  y=31.799837369996,  heading=math.deg(1.850049007114)},
        },
    }
    self:spawnStatics{
        namePrefix=self.staticPrefixMassClearDeck,
        type="us carrier tech",
        category="Personnel",
        shape="carrier_tech_USA",
        livery="purple",
        positions={
            {x=60.15744568099,   y=36.657607259986, heading=math.deg(5.9341194567807) },
            {x=67.356309497001,  y=32.502165549959, heading=math.deg(2.460914245312)  },
        },
    }
    self:spawnStatics{
        namePrefix=self.staticPrefixMassClearDeck,
        type="us carrier tech",
        category="Personnel",
        shape="carrier_tech_USA",
        livery="yellow",
        positions={
            { x=13.844755134996,  y=24.753144659975,  heading=math.deg(5.218534463463), },
            { x=-19.085825937,    y=19.767758169968,  heading=math.deg(1.5882496193148),},
            { x=-2.3569001290016, y=17.940128899994,  heading=math.deg(1.0471975511966),},
            { x=-111.053451471,   y=32.040782110009,  heading=math.deg(4.6425758103049),},
        },
    }
    self:spawnStatics{
        namePrefix=self.staticPrefixMassClearDeck,
        type="us carrier tech",
        category="Personnel",
        shape="carrier_tech_USA",
        livery="green",
        positions={
            {x=-89.908392819008, y=-33.335796030005, heading=math.deg(3.6651914291881)},
        },
    }
end
-- }}}2

function ScLivelyDeck:spawnMassLaunch() -- {{{2
    -- Function that spawns the mass launch statics.
    local positions = {
        { x=-158.547306564, y=-5.8173064400326, heading=math.deg(1.2566370614359), },
        { x=-148.360713827, y=-8.4159270400414, heading=math.deg(1.2566370614359), },
        { x=-138.48595556,  y=-11.118492459995, heading=math.deg(1.2566370614359), },
        { x=-127.467804232, y=-13.301333760028, heading=math.deg(0.97738438111682),},
        { x=-151.790893014, y=8.8389137199847,  heading=math.deg(6.1261056745001), },
    }
    self:spawnStatics{
        namePrefix=self.staticPrefixMassLaunch,
        type=self.aircraft1,
        category="Planes",
        livery=self.aircraft1Livery,
        positions=positions,
    }
end
-- }}}2

function ScLivelyDeck:spawnFlexFantail() -- {{{2
    --  Spawn aircraft on rear of carrier, but still allowing recovery
    --  Note on headings:
    --  -   0 = heading of ship
    --  -   90 = looking right
    --  -   180 = looking back
    --  -   270 = loking left
    --  Note on positions:
    --  -   x   = center is 0, decreases toward rear, increases toward front
    --  -   y   = center is 0, decreases toward left, increases toward right
    self:spawnStatics{
        namePrefix=self.staticPrefixMassLaunch,
        type=self.aircraft1,
        category="Planes",
        livery=self.aircraft1Livery,
        positions={
            { x=-121.48595556,  y=-15.5,   heading=13, },
            { x=-140.360713827, y=-13.128, heading=14, },
            { x=-159.7,         y=-12.117, heading=28, },
        },
    }

end
-- }}}2

function ScLivelyDeck:spawnMassRecoveryCat1() -- {{{2
    -- Function that spawns the mass recovery statics.
    -- Aircraft on CAT 1.
    local positions = {
        { x=157.17722058357, y=9.4148222083459, heading=math.deg(6.1784655520599) },
        { x=139.74341288069, y=9.4148222091026, heading=math.deg(5.8643062867009) },
        { x=124.83291945075, y=11.173495793249, heading=math.deg(5.7072266540215) },
        { x=110.68706670946, y=12.243992757576, heading=math.deg(5.7246799465414) },
        { x=93.329723075571, y=14.079130410682, heading=math.deg(5.6199601914217) },
        { x=77.900436216994, y=17.661255949992, heading=math.deg(5.1784655520599) },
        { x=63.508736215706, y=18.284654198971, heading=math.deg(5.6025068989018) },
    }
    self:spawnStatics{
        namePrefix=self.staticPrefixMassRecoveryCat1,
        type=self.aircraft1,
        category="Planes",
        livery=self.aircraft1Livery,
        positions=positions,
    }
end
-- }}}2

function ScLivelyDeck:spawnMassRecoveryCat2() -- {{{2
    -- Function that spawns the mass recovery statics.
    -- Aircraft on CAT 2.
    local positions = {
        {x=154.99358265282, y=-8.4167411836679, heading=math.deg(0.27925268031909), },
        {x=143.11477288499, y=-7.7287177100079, heading=math.deg(0.73303828583762), },
        {x=130.918647436,   y=-7.7287177100079, heading=math.deg(0.50614548307836), },
        {x=117.83406251699, y=-8.8594843100291, heading=math.deg(0.83775804095728), },
        {x=106.88254922599, y=-8.95903232001,   heading=math.deg(0.82030474843733), },
        {x=94.89565438799,  y=-7.3248725000303, heading=math.deg(0.59341194567807), },
        {x=81.164917126996, y=-5.3056464300025, heading=math.deg(0.68067840827779), },
        {x=64.930339543003, y=-3.2056513199932, heading=math.deg(0.36651914291881), },
    }
    self:spawnStatics{
        namePrefix=self.staticPrefixMassRecoveryCat2,
        type=self.aircraft1,
        category="Planes",
        livery=self.aircraft1Livery,
        positions=positions,
    }
end
-- }}}2

function ScLivelyDeck:spawnMassRecovery() -- {{{2
    -- Function that spawns the mass recovery statics.
    self:spawnMassRecoveryCat1()
    self:spawnMassRecoveryCat2()
end
-- }}}2

function ScLivelyDeck:setDeckForLaunching() -- {{{2
    self:wipeStatics()
    self:spawnClearDeck()
    self:spawnBlockedDeck()
    self:spawnMassLaunch()
end
-- }}}2

function ScLivelyDeck:setDeckForRecovery() -- {{{2
    self:wipeStatics()
    self:spawnClearDeck()
    self:spawnMassRecoveryCat1()
    self:spawnMassRecoveryCat2()
end
-- }}}2

function ScLivelyDeck:setFlexDeck0() -- {{{2
    self:wipeStatics()
    self:spawnClearDeck()
    self:spawnFlexFantail()
end
-- }}}2
--
function ScLivelyDeck:setFlexDeck1() -- {{{2
    self:wipeStatics()
    self:spawnClearDeck()
    self:spawnFlexFantail()
    self:spawnMassRecoveryCat2()
end
-- }}}2

function ScLivelyDeck:setFlexDeck2() -- {{{2
    self:wipeStatics()
    self:spawnClearDeck()
    self:spawnFlexFantail()
    self:spawnMassRecoveryCat1()
end
-- }}}2


function ScLivelyDeck:setupMenuCommands(coalitionId, submenuPath) -- {{{2
    missionCommands.addCommandForCoalition(coalitionId, "Set Deck for Launching", submenuPath, self.setDeckForLaunching, self)
    missionCommands.addCommandForCoalition(coalitionId, "Set Deck for Recovery", submenuPath, self.setDeckForRecovery, self)
    missionCommands.addCommandForCoalition(coalitionId, "Set Flex Deck", submenuPath, self.setFlexDeck2, self)
    -- missionCommands.addCommandForCoalition(coalitionId, "(Add) Clear", submenuPath, self.spawnClearDeck, self)
    -- missionCommands.addCommandForCoalition(coalitionId, "(Add) Blocked", submenuPath, self.spawnBlockedDeck, self)
    -- missionCommands.addCommandForCoalition(coalitionId, "(Add) Mass Launch", submenuPath, self.spawnMassLaunch, self)
    -- missionCommands.addCommandForCoalition(coalitionId, "(Add) Mass Recovery", submenuPath, self.spawnMassRecovery, self)
    missionCommands.addCommandForCoalition(coalitionId, "Remove all", submenuPath, self.wipeStatics, self)
end
-- }}}2

-- }}}1

-- cobralake.CarrierOps  {{{1
----------------------------------------------------------------------------------

cobralake.CarrierOps = {}
cobralake.CarrierOps.__index = cobralake.CarrierOps

setmetatable(cobralake.CarrierOps, {
    __call = function (cls, ...)
        return cls.new(...)
    end,
})

function cobralake.CarrierOps.new(init)
    local self = setmetatable({}, cobralake.CarrierOps)
    self.coalitionId = init["coalitionId"]
    _assertNotNil(self, "coalitionId")
    self.carrierUnit = init["carrierUnit"]
    if self.carrierUnit == nil then
        self.carrierUnitName = init["carrierUnitName"]
        self.carrierUnit = UNIT:FindByName(self.carrierUnitName)
    else
        self.carrierUnitName = self.carrierUnit:Name()
    end
    _assertNotNil(self, "carrierUnit")
    self.deckManager = ScLivelyDeck{
        carrierUnitName=self.carrierUnitName
    }
    self.deckManager:setFlexDeck0()
    self.tankerGroupName = init["tankerGroupName"]

    capControlInit = init["capControl"]
    if capControlInit ~= nil then
        capControlInit["homeAirbaseName"] = self.carrierUnitName
        capControlInit["coalitionId"] = self.coalitionId
        self.capControl = cobralake.CapControl(capControlInit)
        capStationDefinitions = init["capStationDefinitions"]
        if capStationDefinitions ~= nil then
            for _, capStationInit in pairs(capStationDefinitions) do
                self.capControl:newCapStation(capStationInit)
            end
        end
    else
        self.capControl = nil
    end


    return self
end

function cobralake.CarrierOps:start()
    self:startRecoveryTanker()
    if self.capControl ~= nil then
        self.capControl:startCombatAirPatrols()
    end
    -- Set carrier strike groups to patrol waypoints indefinitely. Once the
    -- last waypoint is reached, group will go back to first waypoint and start
    -- over.
    UNIT:FindByName(self.carrierUnitName):PatrolRoute()
end

function cobralake.CarrierOps:startRecoveryTanker()
    -- TANKER
    -- FROM: https://forums.eagle.ru/showthread.php?t=231022
    --  Reference: https://flightcontrol-master.github.io/MOOSE_DOCS_DEVELOP/Documentation/Ops.RecoveryTanker.html
    if self.tankerGroupName ~= nil then
        local tankerCV=RECOVERYTANKER:New(self.carrierUnitName, self.tankerGroupName)
        -- tankerCV:SetRadio(self.carrierTankerRadio)
        -- tankerCV:SetCallsign(self.carrierTankerCallsign)
        -- tankerCV:SetTACAN(self.carrierTankerTacan[1], self.carrierTankerTacan[2])
        -- tankerCV:SetModex(0)
        -- if self.carrierTankerStart == "air" then
        tankerCV:SetTakeoffAir()
        tankerCV:SetAltitude(6000)
        tankerCV:SetSpeed(220)
        tankerCV:SetRacetrackDistances(40, 20)
        -- end
        -- NOTE: If you spawn on deck, it seems prudent to delay the spawn a bit after the mission starts.
        tankerCV:__Start(1)
    end
    -- (We do not use above approach for AWACS, because this puts the AWACS in
    -- orbit above or behind carrier, rather than far out

end

-- }}}1


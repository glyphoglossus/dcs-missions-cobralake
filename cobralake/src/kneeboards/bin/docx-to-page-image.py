#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import pathlib
import sys
import argparse
import subprocess
import shutil
import tempfile
import zipfile

def _log(s):
    sys.stderr.write("- {}\n".format(s))

def _execute(cmd):
    _log("Executing:\n    {}".format(" ".join(str(s) for s in cmd)))
    subprocess.run(cmd,
            # capture_output=True,
            # shell=True,
            # check=True,
            )
    _log("OK")

def main():
    parser = argparse.ArgumentParser(description=None)
    parser.add_argument("source_docx_file",
            action="store",
            help="Path to DOCX source file")
    parser.add_argument("-o", "--output-dir",
            action="store",
            default="build",
            help="Path to output directory (defaults to '%(default)s').")
    args = parser.parse_args()

    if True:
        doc_path = pathlib.Path(os.path.expanduser(os.path.expandvars(args.source_docx_file)))
        output_dir = pathlib.Path(os.path.expanduser(os.path.expandvars(args.output_dir)))
        os.makedirs(output_dir, exist_ok=True)
        with tempfile.TemporaryDirectory() as tempdir:
            pdf_path = os.path.join(tempdir, doc_path.stem + ".pdf")
            img_path = os.path.join(output_dir, doc_path.stem + ".png")
            _log("Converting to PDF ...")
            _execute(["soffice", "--convert-to", "pdf", doc_path, "--outdir", tempdir])
            _log("Converting PNG ...")
            # _execute(["convert", pdf_path, "-resize", "1033x1469", "-background", "white", "-alpha", "remove", "-alpha", "off", "-flatten", img_path])
            # _execute(["convert", pdf_path, "-resize", "1033x1469", img_path])
            _execute(["convert", pdf_path,
                    "-resize", "1033x1469",
                    "-background", "white",
                    "-alpha", "remove",
                    "-alpha", "off",
                    "-flatten",
                    # "-colorspace", "sRGB",
                    img_path])

    if False:
        doc_path = pathlib.Path(os.path.expanduser(os.path.expandvars(args.source_docx_file)))
        output_dir = pathlib.Path(os.path.expanduser(os.path.expandvars(args.output_dir)))
        os.makedirs(output_dir, exist_ok=True)
        img_path = os.path.join(output_dir, doc_path.stem + ".png")
        _log("Converting to PNG ...")
        _execute(["soffice", "--convert-to", "png", doc_path, "--outdir", output_dir])
        _log("Resizing PNG ...")
        # "-colorspace XXX" if image isn't already in colorspace XXX (according to the metadata), this changes pixel values and metadata.
        # "-set colorspace XXX" if image isn't already in colorspace XXX (according to the metadata), this changes metadata only.
        _execute(["convert", img_path, "-resize", "1033x1469", "PNG00:"+img_path])
        # _execute(["convert", pdf_path, "-resize", "1033x1469", "-background", "white", "-alpha", "remove", "-alpha", "off", "-flatten", img_path])

if __name__ == '__main__':
    main()

#! /bin/bash

set -e -o pipefail

if [ -z "$1" ]
then
    echo "Path to (plain) text file required as argument"
    exit 1
fi

export src="$1"
# export filename=$(basename -- "$src")
export filename="${src##*/}"
export extension="${filename##*.}"
export filename="${filename%.*}"
echo $filename

pandoc -o ${filename}.docx $src
soffice --convert-to jpg ${filename}.docx
convert ${filename}.jpg -resize 1033x1469 ${filename}.jpg

#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import pathlib
import sys
import argparse
import subprocess
import shutil
import tempfile
import zipfile
import re

def _log(s):
    sys.stderr.write("- {}\n".format(s))

def _execute(cmd):
    _log("Executing:\n    {}".format(" ".join(str(s) for s in cmd)))
    subprocess.run(cmd,
            # capture_output=True,
            # shell=True,
            # check=True,
            )
    _log("OK")

def remove_from_zip(zipfname, exclude_pattern):
    with tempfile.TemporaryDirectory() as tempdir:
        tempname = os.path.join(tempdir, 'new.zip')
        with zipfile.ZipFile(zipfname, 'r') as zipread:
            with zipfile.ZipFile(tempname, 'w') as zipwrite:
                for item in zipread.infolist():
                    if not exclude_pattern.match(item.filename):
                        print("Including: {}".format(item.filename))
                        data = zipread.read(item.filename)
                        zipwrite.writestr(item, data)
                    else:
                        print("Excluding: {}".format(item.filename))
        shutil.move(tempname, zipfname)

def main():
    parser = argparse.ArgumentParser(description=None)
    parser.add_argument("source_dir",
            action="store",
            help="Path to kneeboard source direction")
    parser.add_argument("dcs_miz_file",
            action="store",
            help="Path to DCS mission file")
    args = parser.parse_args()

    src_dir = pathlib.Path(os.path.expanduser(os.path.expandvars(args.source_dir)))
    miz_path = pathlib.Path(os.path.expanduser(os.path.expandvars(args.dcs_miz_file)))

    kneeboard_root_path_str = "KNEEBOARD/IMAGES"
    remove_from_zip(miz_path, re.compile(kneeboard_root_path_str))
    kneeboard_root_path = pathlib.Path(kneeboard_root_path_str)
    with zipfile.ZipFile(miz_path, "a") as z:
        # for src_path in src_dir.glob("**/*"):
        for src_path in src_dir.iterdir():
            dest_path = kneeboard_root_path / src_path.relative_to(src_dir)
            print("Adding: {} => {}".format(src_path, dest_path))
            z.write(src_path, dest_path)

if __name__ == '__main__':
    main()
